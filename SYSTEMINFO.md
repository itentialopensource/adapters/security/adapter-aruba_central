# Aruba Central

Vendor: Aruba Networks
Homepage: https://www.hpe.com/us/en/home.html

Product: Central
Product Page: https://www.hpe.com/us/en/aruba-central.html

## Introduction
We classify Aruba Central into the Security domain as Aruba Central enables security-related functionalities and features within the Aruba Central platform.

"Aruba Central established the right network foundation for zero-trust security and SASE architectures while ensuring users have fast, secure access no matter how or where they connect." 

## Why Integrate
The Aruba Central adapter from Itential is used to integrate the Itential Automation Platform (IAP) with HPE Aruba Networking Central. 

With this adapter you have the ability to perform operations with Aruba Central such as:

- Create Site
- Get Site
- Get Network
- Get Transport Profile
- List Access Points
- Assign License
- Get Network Health

## Additional Product Documentation
The [API documents for Aruba Central](https://developer.arubanetworks.com/aruba-central/docs/api-gateway)