## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Aruba Central. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Aruba Central.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Aruba_central. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">ccsApigwSsoManagementSamlSsoGetMetadata(domain, callback)</td>
    <td style="padding:15px">Get saml metadata</td>
    <td style="padding:15px">{base_path}/{version}/platform/aaa_config/v2/authentication/profiles/metadata/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ccsApigwSsoManagementSamlSsoGetDomainList(callback)</td>
    <td style="padding:15px">Get domain list</td>
    <td style="padding:15px">{base_path}/{version}/platform/aaa_config/v2/authentication/idp/source?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ccsApigwSsoManagementSamlSsoAddAuthenticationSource(idpconfig, callback)</td>
    <td style="padding:15px">Add IDP config</td>
    <td style="padding:15px">{base_path}/{version}/platform/aaa_config/v2/authentication/idp/source?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ccsApigwSsoManagementSamlSsoUpdateIdpConfig(samlIdpConfig, callback)</td>
    <td style="padding:15px">Update IDP configuration</td>
    <td style="padding:15px">{base_path}/{version}/platform/aaa_config/v2/authentication/idp/source?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ccsApigwSsoManagementSamlSsoDeleteDomain(domain, callback)</td>
    <td style="padding:15px">Un-claim domain</td>
    <td style="padding:15px">{base_path}/{version}/platform/aaa_config/v2/authentication/idp/source/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ccsApigwSsoManagementSamlSsoExtractMetadata(samlMetaData, callback)</td>
    <td style="padding:15px">Extract Saml metadata from file</td>
    <td style="padding:15px">{base_path}/{version}/platform/aaa_config/v2/authentication/idp/source/extract/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAiopsV1ConnectivityGlobalStageStageExport(stage = 'all', from, to, callback)</td>
    <td style="padding:15px">Wi-Fi Connectivity at Global</td>
    <td style="padding:15px">{base_path}/{version}/aiops/v1/connectivity/global/stage/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAiopsV1ConnectivitySiteSiteIdStageStageExport(siteId, stage = 'all', from, to, callback)</td>
    <td style="padding:15px">Wi-Fi Connectivity at Site</td>
    <td style="padding:15px">{base_path}/{version}/aiops/v1/connectivity/site/{pathv1}/stage/{pathv2}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAiopsV1ConnectivityGroupGroupStageStageExport(group, stage = 'all', from, to, callback)</td>
    <td style="padding:15px">Wi-Fi Connectivity at Group</td>
    <td style="padding:15px">{base_path}/{version}/aiops/v1/connectivity/group/{pathv1}/stage/{pathv2}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAiopsV2InsightsGlobalList(from, to, callback)</td>
    <td style="padding:15px">List AI Insights for a Network</td>
    <td style="padding:15px">{base_path}/{version}/aiops/v2/insights/global/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAiopsV2InsightsSiteSiteIdList(siteId, from, to, callback)</td>
    <td style="padding:15px">List AI Insights for a Site</td>
    <td style="padding:15px">{base_path}/{version}/aiops/v2/insights/site/{pathv1}/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAiopsV2InsightsApApSerialList(apSerial, from, to, callback)</td>
    <td style="padding:15px">List AI Insights for an AP</td>
    <td style="padding:15px">{base_path}/{version}/aiops/v2/insights/ap/{pathv1}/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAiopsV2InsightsClientStaMacList(staMac, from, to, callback)</td>
    <td style="padding:15px">List AI Insights for a Client</td>
    <td style="padding:15px">{base_path}/{version}/aiops/v2/insights/client/{pathv1}/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAiopsV2InsightsGatewayGwSerialList(gwSerial, from, to, callback)</td>
    <td style="padding:15px">List AI Insights for a Gateway</td>
    <td style="padding:15px">{base_path}/{version}/aiops/v2/insights/gateway/{pathv1}/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAiopsV2InsightsSwitchSwSerialList(swSerial, from, to, callback)</td>
    <td style="padding:15px">List AI Insights for a Switch</td>
    <td style="padding:15px">{base_path}/{version}/aiops/v2/insights/switch/{pathv1}/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAiopsV2InsightsGlobalIdInsightIdExport(insightId, from, to, callback)</td>
    <td style="padding:15px">AI Insight Details for a Network</td>
    <td style="padding:15px">{base_path}/{version}/aiops/v2/insights/global/id/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAiopsV2InsightsSiteSiteIdIdInsightIdExport(siteId, insightId, from, to, callback)</td>
    <td style="padding:15px">AI Insight Details for a Site</td>
    <td style="padding:15px">{base_path}/{version}/aiops/v2/insights/site/{pathv1}/id/{pathv2}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAiopsV2InsightsApApSerialIdInsightIdExport(apSerial, insightId, from, to, callback)</td>
    <td style="padding:15px">AI Insight Details for an AP</td>
    <td style="padding:15px">{base_path}/{version}/aiops/v2/insights/ap/{pathv1}/id/{pathv2}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAiopsV2InsightsClientStaMacIdInsightIdExport(staMac, insightId, from, to, callback)</td>
    <td style="padding:15px">AI Insight Details for a Client</td>
    <td style="padding:15px">{base_path}/{version}/aiops/v2/insights/client/{pathv1}/id/{pathv2}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAiopsV2InsightsGatewayGwSerialIdInsightIdExport(gwSerial, insightId, from, to, callback)</td>
    <td style="padding:15px">AI Insight Details for a Gateway</td>
    <td style="padding:15px">{base_path}/{version}/aiops/v2/insights/gateway/{pathv1}/id/{pathv2}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAiopsV2InsightsSwitchSwSerialIdInsightIdExport(swSerial, insightId, from, to, callback)</td>
    <td style="padding:15px">AI Insight Details for a Switch</td>
    <td style="padding:15px">{base_path}/{version}/aiops/v2/insights/switch/{pathv1}/id/{pathv2}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsGetAirgroupTrafficSummary(startTime, endTime, label, callback)</td>
    <td style="padding:15px">Get AirGroup Traffic Summary in terms of Packets</td>
    <td style="padding:15px">{base_path}/{version}/airgroup/v1/traffic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsGetAirgroupDeviceSummary(callback)</td>
    <td style="padding:15px">Retrieves device summary</td>
    <td style="padding:15px">{base_path}/{version}/airgroup/v1/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiGetApiGuestStatistics(days, ssid, callback)</td>
    <td style="padding:15px">Get summary statistics</td>
    <td style="padding:15px">{base_path}/{version}/guest/v1/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ucDshbViewsGetUcSummary(label, startTime, endTime, callback)</td>
    <td style="padding:15px">Fetch Summary</td>
    <td style="padding:15px">{base_path}/{version}/v1/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsGetAirgroupTrends(startTime, endTime, trendType, label, callback)</td>
    <td style="padding:15px">Get temporal data about AirGroup based on the parameter passed</td>
    <td style="padding:15px">{base_path}/{version}/airgroup/v1/trend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsGetLabelListByCid(callback)</td>
    <td style="padding:15px">Retrieves list of labels</td>
    <td style="padding:15px">{base_path}/{version}/airgroup/v1/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsGetAirgroupUncachedServiceid(callback)</td>
    <td style="padding:15px">Get all the uncached services encountered by AirGroup</td>
    <td style="padding:15px">{base_path}/{version}/airgroup/v1/uncached_services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsGetAirgroupHostname(callback)</td>
    <td style="padding:15px">Retrieves a list of all hostnames</td>
    <td style="padding:15px">{base_path}/{version}/airgroup/v1/hostnames?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsGetAirgroupServiceQuerySummary(startTime, endTime, label, callback)</td>
    <td style="padding:15px">Retrieves a summary of all the services queried for.</td>
    <td style="padding:15px">{base_path}/{version}/airgroup/v1/distribution/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsGetAirgroupServerDistribution(startTime, endTime, label, callback)</td>
    <td style="padding:15px">Retrieves a summary of the servers connected to AirGroup</td>
    <td style="padding:15px">{base_path}/{version}/airgroup/v1/distribution/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsGetAirgroupSuppressionFactor(callback)</td>
    <td style="padding:15px">Retrieves the suppression factor</td>
    <td style="padding:15px">{base_path}/{version}/airgroup/v1/stats/suppression?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryGetRepRadioByRadioMac(tenantId, radioMac, callback)</td>
    <td style="padding:15px">Get reporting_radio of a specific Radio MAC</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/reporting_radio/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryGetAllRepRadio(tenantId, callback)</td>
    <td style="padding:15px">Get All reporting_radio for a Customer</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/reporting_radio_all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryGetApInfoByEthMac(tenantId, apEthMac, callback)</td>
    <td style="padding:15px">Get ap_info of a specific AP Ethernet MAC</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/ap_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryGetAllApInfo(tenantId, callback)</td>
    <td style="padding:15px">Get ap_info for all APs</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/ap_info_all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersFeasibilityGetApInfoBySerial(tenantId, apSerial, callback)</td>
    <td style="padding:15px">Get feasibility ap_info of a specific AP Ethernet MAC</td>
    <td style="padding:15px">{base_path}/{version}/feasibility/v1/ap_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryGetNbrPathlossByNbrBand(tenantId, radioMac, nbrMac, band, callback)</td>
    <td style="padding:15px">Get nbr_pathloss of a Neighbor Mac heard by a specific Radio Mac</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/nbr_pathloss/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryGetAllNbrPathloss(tenantId, band, callback)</td>
    <td style="padding:15px">Get All nbr_pathloss for a Customer and Band</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/nbr_pathloss_all/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryGetRadioAllNbrPathloss(tenantId, radioMac, band, callback)</td>
    <td style="padding:15px">Get All nbr_pathloss for a Customer and Radio-Mac</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/nbr_pathloss_radio/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryGetRfEventsByRadioMac(tenantId, radioMac, callback)</td>
    <td style="padding:15px">Get rf_events of a specific Radio MAC</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/rf_events/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryGetAllRfEvents(tenantId, callback)</td>
    <td style="padding:15px">Get all rf_events of a tenant-id</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/rf_events_all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryGetPriorityRfEventsByRadioMac(tenantId, radioMac, callback)</td>
    <td style="padding:15px">Get radar and noise RF events of a specific Radio MAC</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/priority_rf_events/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryGetAllPriorityRfEvents(tenantId, callback)</td>
    <td style="padding:15px">Get all radar and noise RF events of a tenant</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/priority_rf_events_all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryGetHistoryByRadioMac(tenantId, radioMac, band, callback)</td>
    <td style="padding:15px">Get history of a specific Radio MAC</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/history/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryGetAllStaticRadios(tenantId, callback)</td>
    <td style="padding:15px">Get All Static Radios for a Customer</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/static_radio_all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryGetAdvancedStatAp(tenantId, callback)</td>
    <td style="padding:15px">Get Number of APs and AP Models</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/adv_ap_stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryGetAdvancedStatEirp(tenantId, callback)</td>
    <td style="padding:15px">Get EIRP Distribution</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/adv_eirp_distrubution?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryGetAdvancedStatEirpReason(tenantId, callback)</td>
    <td style="padding:15px">Get EIRP Reasons</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/adv_eirp_reason?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersRecieverGetAdvancedStatEirpFeasibleRange(tenantId, callback)</td>
    <td style="padding:15px">Get EIRP Reasons</td>
    <td style="padding:15px">{base_path}/{version}/receiver/v1/adv_eirp_range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryGetAdvancedStatRadio(tenantId, callback)</td>
    <td style="padding:15px">Get Information about Radio</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/adv_stat_radio?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryGetAdvStatNbr(tenantId, callback)</td>
    <td style="padding:15px">Get Neighbor stats information</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/adv_stat_nbr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryBootstrap(tenantId, bootstrapType, callback)</td>
    <td style="padding:15px">Bootstrap</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/bootstrap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersTelemetryPurge(tenantId, purgeType, callback)</td>
    <td style="padding:15px">Purge</td>
    <td style="padding:15px">{base_path}/{version}/telemetry/v1/purge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersSolverProcessOptimizationGetReq(tenantId, callback)</td>
    <td style="padding:15px">Get optimizations for tenant</td>
    <td style="padding:15px">{base_path}/{version}/solver/v1/optimization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersSolverProcessOptimizationPostReq(tenantId, callback)</td>
    <td style="padding:15px">run the algorithm for the solution</td>
    <td style="padding:15px">{base_path}/{version}/solver/v1/optimization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersSolverGetRadioPlanByRadioMac(tenantId, radioMac, debug, callback)</td>
    <td style="padding:15px">Get solution of a specific Radio MAC</td>
    <td style="padding:15px">{base_path}/{version}/solver/v1/radio_plan/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersSolverGetAllRadioPlan(tenantId, band = '2.4ghz', debug, callback)</td>
    <td style="padding:15px">Get All solutions for a Customer</td>
    <td style="padding:15px">{base_path}/{version}/solver/v1/radio_plan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersSolverGetOptimizationPerPartition(tenantId, rfId, partitionId, callback)</td>
    <td style="padding:15px">Get optimizations for tenant's requested partition</td>
    <td style="padding:15px">{base_path}/{version}/solver/v1/optimization_partition/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersSolverGetAdvStateDeployment(tenantId, callback)</td>
    <td style="padding:15px">Gets Radios Deployment Status</td>
    <td style="padding:15px">{base_path}/{version}/solver/v1/advanced_deployment_stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersSchedulerProcessTenantSvcConfigUpdate(tenantId, callback)</td>
    <td style="padding:15px">RMQ message triggers a recompute of the schedule due to change in tenant timezone/deploy hour detai</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/v1/svc-config-update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersSchedulerProcessTriggerRunnow(tenantId, runnowType, callback)</td>
    <td style="padding:15px">RMQ message triggers a runnow job for a certain tenant</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/v1/runnow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersSchedulerProcessGetSchedule(tenantId, callback)</td>
    <td style="padding:15px">get the schedule of all jobs computed by the scheduler</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/v1/schedule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersSchedulerProcessGetDeployJobs(tenantId, callback)</td>
    <td style="padding:15px">get the jobs to be sent to deployer for airmatch solution deployment</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/v1/deploy-jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersSchedulerProcessGetJobList(tenantId, callback)</td>
    <td style="padding:15px">Get the list of jobs generated by Scheduler</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/v1/job-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersSchedulerProcessGetTenantTzDeployHrInfo(tenantId, callback)</td>
    <td style="padding:15px">Get the list of unique timezone and deploy hours per tenant</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/v1/tenant-tz-deploy-hr-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersSchedulerProcessTriggerSolverJob(tenantId, callback)</td>
    <td style="padding:15px">Trigger - RMQ message with on-demand compute for a provided tenant-id</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/v1/trigger-solver-job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersRecieverUpdateFeasibility(tenantId, radioMac, callback)</td>
    <td style="padding:15px">Trigger update of radio feasibility</td>
    <td style="padding:15px">{base_path}/{version}/receiver/v1/radio_feasibility_update/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersRecieverGetRadioFeasByRadioMac(tenantId, radioMac, callback)</td>
    <td style="padding:15px">Get radio_feasibility of a specific radio MAC</td>
    <td style="padding:15px">{base_path}/{version}/receiver/v1/radio_feasibility/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersRecieverGetAllRadioFeas(tenantId, callback)</td>
    <td style="padding:15px">Get All radio_feasibility for a Customer</td>
    <td style="padding:15px">{base_path}/{version}/receiver/v1/radio_feasibility_all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersFeasibilityGetRadioFeasByRadioMac(tenantId, radioMac, callback)</td>
    <td style="padding:15px">Get radio_feasibility of a specific radio MAC</td>
    <td style="padding:15px">{base_path}/{version}/feasibility/v1/radio_feasibility/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersFeasibilityGetAllRadioFeas(tenantId, callback)</td>
    <td style="padding:15px">Get All radio_feasibility for a Customer</td>
    <td style="padding:15px">{base_path}/{version}/feasibility/v1/radio_feasibility?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersRecieverGetAllDeviceConfig(tenantId, callback)</td>
    <td style="padding:15px">Returns all Device (AP) Running Configuration for a Customer</td>
    <td style="padding:15px">{base_path}/{version}/receiver/v1/devices_config_all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersRecieverGetDeviceConfig(tenantId, apSerial, callback)</td>
    <td style="padding:15px">Returns Device (AP) Running Configuration</td>
    <td style="padding:15px">{base_path}/{version}/receiver/v1/device_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersRecieverSetDeviceConfig(tenantId, apSerial, body, callback)</td>
    <td style="padding:15px">Change a device Running Config</td>
    <td style="padding:15px">{base_path}/{version}/receiver/v1/device_config_set/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersFeasibilityGetDeviceConfig(tenantId, apSerial, callback)</td>
    <td style="padding:15px">Returns Device (AP) Running Configuration</td>
    <td style="padding:15px">{base_path}/{version}/feasibility/v1/device_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersFeasibilitySetDeviceConfig(tenantId, apSerial, body, callback)</td>
    <td style="padding:15px">Change a device Running Config</td>
    <td style="padding:15px">{base_path}/{version}/feasibility/v1/device_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersFeasibilityGetAllDeviceConfig(tenantId, callback)</td>
    <td style="padding:15px">Returns all Device (AP) Running Configuration for a Customer</td>
    <td style="padding:15px">{base_path}/{version}/feasibility/v1/device_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersRecieverGetAllServiceConfig(callback)</td>
    <td style="padding:15px">Returns All Device (AP) Running Configuration for all customers</td>
    <td style="padding:15px">{base_path}/{version}/receiver/v1/service_config_all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersRecieverGetServiceConfig(tenantId, callback)</td>
    <td style="padding:15px">Returns Device (AP) Running Configuration</td>
    <td style="padding:15px">{base_path}/{version}/receiver/v1/service_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersFeasibilityGetFeasRadioInfo(tenantId, radioMac, callback)</td>
    <td style="padding:15px">Get Feasibility Radio info of a specific radio MAC</td>
    <td style="padding:15px">{base_path}/{version}/feasibility/v1/radio_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersFeasibilityGetRadioBoardLimit(tenantId, radioMac, callback)</td>
    <td style="padding:15px">Get board limits of a specific radio MAC</td>
    <td style="padding:15px">{base_path}/{version}/feasibility/v1/board_limit/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersDeployerProcessGetPendingDeployments(tenantId, deployHour, callback)</td>
    <td style="padding:15px">get a list of pending deployments for a tenant-id</td>
    <td style="padding:15px">{base_path}/{version}/deployer/v1/pending_deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersDeployerProcessTrigerComputationComplete(tenantId, callback)</td>
    <td style="padding:15px">Trigger Computation complete message</td>
    <td style="padding:15px">{base_path}/{version}/deployer/v1/trigger_computation_complete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersDeployerProcessTestActionMsg(tenantId, mac, opmode, cbw, chan, eirp, callback)</td>
    <td style="padding:15px">RMQ message generates southbound test action-msg</td>
    <td style="padding:15px">{base_path}/{version}/deployer/v1/trigger_test_action_msg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersDeployerProcessTestConfig(tenantId, disallowActionMsg, callback)</td>
    <td style="padding:15px">Trigger test-config update</td>
    <td style="padding:15px">{base_path}/{version}/deployer/v1/test_config_update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersApNbrGraphProcessApNeighborsGetReq(tenantId, apserialnum, count, maxPathloss, apMac, callback)</td>
    <td style="padding:15px">Get AP neighbor list</td>
    <td style="padding:15px">{base_path}/{version}/ap_nbr_graph/v1/Ap/NeighborList/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersApNbrGraphProcessApUpgradeSamplingGetReq(tenantId, aplist, callback)</td>
    <td style="padding:15px">Get AP neighbor list</td>
    <td style="padding:15px">{base_path}/{version}/ap_nbr_graph/v1/Ap/LiveUpgrade/Sampling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetApsV2(group, swarmId, label, site, status, serial, macaddr, model, clusterId, fields, calculateTotal, calculateClientCount, calculateSsidCount, showResourceDetails, offset, limit, sort, callback)</td>
    <td style="padding:15px">List Access Points</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v2/aps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetBssidsV2(group, swarmId, label, site, serial, macaddr, clusterId, calculateTotal, offset, limit, sort, callback)</td>
    <td style="padding:15px">List BSSIDs</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v2/bssids?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetAp(serial, callback)</td>
    <td style="padding:15px">AP Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/aps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerDeleteAp(serial, callback)</td>
    <td style="padding:15px">Delete AP</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/aps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetApRfSummaryV3(serial, band, radioNumber, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">AP RF Summary</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v3/aps/{pathv1}/rf_summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetApsBandwidthUsageV3(group, swarmId, label, site, serial, clusterId, interval, band, radioNumber, ethernetInterfaceIndex, network, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">AP Bandwidth Usage</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v3/aps/bandwidth_usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetApsBandwidthUsageTopnV2(group, swarmId, label, site, clusterId, count, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">Top N AP Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v2/aps/bandwidth_usage/topn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersApNbrGraphProcessRadioNeighborsGetReq(tenantId, radiomac, callback)</td>
    <td style="padding:15px">Get Radio neighbor list</td>
    <td style="padding:15px">{base_path}/{version}/ap_nbr_graph/v1/Radio/NeighborList/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersApNbrGraphProcessPartitionGetReq(tenantId, band = '2.4ghz', ptype = 'normal', callback)</td>
    <td style="padding:15px">Get partition information</td>
    <td style="padding:15px">{base_path}/{version}/ap_nbr_graph/v1/partition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlersApNbrGraphProcessPartitionPostReq(tenantId, callback)</td>
    <td style="padding:15px">Start partition process</td>
    <td style="padding:15px">{base_path}/{version}/ap_nbr_graph/v1/partition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsApprfApiViewApprfTopNStatsIapGet(count, group, site, swarmId, serial, macaddr, fromTimestamp, toTimestamp, ssids, userRole, details, callback)</td>
    <td style="padding:15px">Gets Top N Apprf Statistics</td>
    <td style="padding:15px">{base_path}/{version}/apprf/v1/topstats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsApprfApiViewGetTopNStatsV2(count, group, groupId, clusterId, labelId, site, metrics, swarmId, serial, macaddr, metricId, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">Gets Top N Apprf Statistics (V2 Version)</td>
    <td style="padding:15px">{base_path}/{version}/apprf/datapoints/v2/topn_stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsApprfApiViewApplicationsGet(count, group, swarmId, serial, macaddr, fromTimestamp, toTimestamp, ssids, userRole, details, callback)</td>
    <td style="padding:15px">Gets Top N Applications</td>
    <td style="padding:15px">{base_path}/{version}/apprf/v1/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsApprfApiViewWebcategoriesGet(count, group, swarmId, serial, macaddr, fromTimestamp, toTimestamp, ssids, userRole, details, callback)</td>
    <td style="padding:15px">Gets Top N Web categories</td>
    <td style="padding:15px">{base_path}/{version}/apprf/v1/webcategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsApprfApiViewAppcategoriesGet(count, group, swarmId, serial, macaddr, fromTimestamp, toTimestamp, ssids, userRole, details, callback)</td>
    <td style="padding:15px">Gets Top N App categories</td>
    <td style="padding:15px">{base_path}/{version}/apprf/v1/appcategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsApprfApiViewWebreputationsGet(group, swarmId, serial, macaddr, fromTimestamp, toTimestamp, ssids, userRole, details, callback)</td>
    <td style="padding:15px">Gets Top Web Reputations</td>
    <td style="padding:15px">{base_path}/{version}/apprf/v1/webreputations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsApprfApiViewWebreputationMappingGet(callback)</td>
    <td style="padding:15px">Gets Web Reputation id to name mapping</td>
    <td style="padding:15px">{base_path}/{version}/apprf/v1/metainfo/iap/webreputation/id_to_name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsApprfApiViewApplicationMappingGet(callback)</td>
    <td style="padding:15px">Gets Application id to name mapping</td>
    <td style="padding:15px">{base_path}/{version}/apprf/v1/metainfo/iap/application/id_to_name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsApprfApiViewAppcategoryMappingGet(callback)</td>
    <td style="padding:15px">Gets Application Category id to name mapping</td>
    <td style="padding:15px">{base_path}/{version}/apprf/v1/metainfo/iap/appcategory/id_to_name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsApprfApiViewWebcategoryMappingGet(callback)</td>
    <td style="padding:15px">Gets Web Category id to name mapping</td>
    <td style="padding:15px">{base_path}/{version}/apprf/v1/metainfo/iap/webcategory/id_to_name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGetAudits(limit, offset, groupName, deviceId, classification, startTime, endTime, callback)</td>
    <td style="padding:15px">Get all audit events for all groups</td>
    <td style="padding:15px">{base_path}/{version}/auditlogs/v1/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGetAuditDetails(id, callback)</td>
    <td style="padding:15px">Get details of an audit event/log</td>
    <td style="padding:15px">{base_path}/{version}/auditlogs/v1/event_details/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGetAuditLogs(limit, offset, username, startTime, endTime, description, target, classification, customerName, ipAddress, appId, callback)</td>
    <td style="padding:15px">Get all audit logs</td>
    <td style="padding:15px">{base_path}/{version}/platform/auditlogs/v1/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGetAuditLogDetails(id, callback)</td>
    <td style="padding:15px">Get details of an audit log</td>
    <td style="padding:15px">{base_path}/{version}/platform/auditlogs/v1/logs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlerBbsNBAPIRetrieveDesireBeacon(profileId, apMac, iotRadioMac, limit, offset, callback)</td>
    <td style="padding:15px">Retrieve ble configured beacons</td>
    <td style="padding:15px">{base_path}/{version}/ble_cfg_beacons/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlerBbsNBAPIRetrieveActualBeacon(profileId, apMac, iotRadioMac, limit, offset, callback)</td>
    <td style="padding:15px">Retrieve ble running beacons</td>
    <td style="padding:15px">{base_path}/{version}/ble_run_beacons/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlerBbsNBAPIRetrieveBeaconProfile(profileId, limit, offset, callback)</td>
    <td style="padding:15px">Retrieve ble beacon profiles</td>
    <td style="padding:15px">{base_path}/{version}/ble_beacon_profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlerBbsNBAPIUpdateDeviceBeacon(iotRadioMac, profileId, beacon, callback)</td>
    <td style="padding:15px">Update one device beacon config</td>
    <td style="padding:15px">{base_path}/{version}/ble_beacon/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHandlerBbsNBAPIDeleteDeviceBeacon(iotRadioMac, profileId, callback)</td>
    <td style="padding:15px">Delete one device beacon config</td>
    <td style="padding:15px">{base_path}/{version}/ble_beacon/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCmEnabledV1TenantId(tenantId, callback)</td>
    <td style="padding:15px">Get the status of Client Match for a tenant</td>
    <td style="padding:15px">{base_path}/{version}/cm-enabled/v1/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCmEnabledV1TenantId(tenantId, requestBody, callback)</td>
    <td style="padding:15px">Enable or disable Client Match for a particular tenant.</td>
    <td style="padding:15px">{base_path}/{version}/cm-enabled/v1/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalEnableV1TenantId(tenantId, callback)</td>
    <td style="padding:15px">Get the status of Client Match Load Balancer for a tenant</td>
    <td style="padding:15px">{base_path}/{version}/loadbal-enable/v1/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLoadbalEnableV1TenantId(tenantId, requestBody, callback)</td>
    <td style="padding:15px">Enable or disable Client Match Load Balancer for a particular tenant.</td>
    <td style="padding:15px">{base_path}/{version}/loadbal-enable/v1/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBandsteer6ghzEnableV1TenantId(tenantId, callback)</td>
    <td style="padding:15px">Get the status of Client Match Band Steer to 6GHz for a tenant</td>
    <td style="padding:15px">{base_path}/{version}/bandsteer-6ghz-enable/v1/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBandsteer6ghzEnableV1TenantId(tenantId, requestBody, callback)</td>
    <td style="padding:15px">Enable or disable Client Match Band Steer to 6GHz for a particular tenant.</td>
    <td style="padding:15px">{base_path}/{version}/bandsteer-6ghz-enable/v1/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUnsteerableV1TenantId(tenantId, callback)</td>
    <td style="padding:15px">Get all unsteerable entries for a tenant</td>
    <td style="padding:15px">{base_path}/{version}/unsteerable/v1/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUnsteerableV1TenantIdClientMac(tenantId, clientMac, callback)</td>
    <td style="padding:15px">Get the unsteerable state of a client</td>
    <td style="padding:15px">{base_path}/{version}/unsteerable/v1/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUnsteerableV1TenantIdClientMac(tenantId, clientMac, requestBody, callback)</td>
    <td style="padding:15px">Set the unsteerable state of a client</td>
    <td style="padding:15px">{base_path}/{version}/unsteerable/v1/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUnsteerableV1TenantIdClientMac(tenantId, clientMac, callback)</td>
    <td style="padding:15px">Delete the unsteerable state of a client</td>
    <td style="padding:15px">{base_path}/{version}/unsteerable/v1/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCloudauthReadAuthAirPassList(limit, cursor, fromTime, timeWindow = '3-hour', callback)</td>
    <td style="padding:15px">Fetch list of authentications using Aruba Air Pass.</td>
    <td style="padding:15px">{base_path}/{version}/auth/air_pass/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCloudauthReadAuthCloudIdentityList(limit, cursor, fromTime, timeWindow = '3-hour', callback)</td>
    <td style="padding:15px">Fetch list of authentications using Cloud Identity.</td>
    <td style="padding:15px">{base_path}/{version}/auth/cloud_identity/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCloudauthReadAuthDetailsRecord(requestId, callback)</td>
    <td style="padding:15px">Fetch details of a specific authentication using either Cloud Identity or Aruba Air Pass.</td>
    <td style="padding:15px">{base_path}/{version}/auth/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCloudauthReadSessionAirPassList(limit, cursor, fromTime, timeWindow = '3-hour', callback)</td>
    <td style="padding:15px">Fetch list of sessions using Aruba Air Pass.</td>
    <td style="padding:15px">{base_path}/{version}/session/air_pass/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCloudauthReadCloudIdentitySessionList(limit, cursor, fromTime, timeWindow = '3-hour', callback)</td>
    <td style="padding:15px">Fetch list of sessions using Cloud Identity.</td>
    <td style="padding:15px">{base_path}/{version}/session/cloud_identity/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCloudauthReadClientPolicy(callback)</td>
    <td style="padding:15px">Fetch network access policy for registered clients.</td>
    <td style="padding:15px">{base_path}/{version}/client_policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCloudauthUpdateClientPolicy(clientPolicy, callback)</td>
    <td style="padding:15px">Configure or update network access policy for registered clients.</td>
    <td style="padding:15px">{base_path}/{version}/client_policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCloudauthDeleteClientPolicy(callback)</td>
    <td style="padding:15px">Delete existing Client Policy.</td>
    <td style="padding:15px">{base_path}/{version}/client_policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCloudauthReadClientRegistration(limit, offset, macPrefix, callback)</td>
    <td style="padding:15px">Fetch list of registered clients.</td>
    <td style="padding:15px">{base_path}/{version}/client_registration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCloudauthAddClientRegistration(clientDetails, callback)</td>
    <td style="padding:15px">Add registered client.</td>
    <td style="padding:15px">{base_path}/{version}/client_registration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCloudauthDeleteClientRegistration(macAddress, callback)</td>
    <td style="padding:15px">Delete registered client.</td>
    <td style="padding:15px">{base_path}/{version}/client_registration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCloudauthUpdateClientRegistration(macAddress, clientDetails, callback)</td>
    <td style="padding:15px">Update registered client name.</td>
    <td style="padding:15px">{base_path}/{version}/client_registration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dppClientRegistrationRead(limit, cursor, id, callback)</td>
    <td style="padding:15px">Fetch list of registered clients.</td>
    <td style="padding:15px">{base_path}/{version}/dpp_registration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dppClientRegistrationAdd(dppClientDetails, callback)</td>
    <td style="padding:15px">Register a client.</td>
    <td style="padding:15px">{base_path}/{version}/dpp_registration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dppClientRegistrationUpdate(id, dppClientDetails, callback)</td>
    <td style="padding:15px">Update registered client with bootstrapping key.</td>
    <td style="padding:15px">{base_path}/{version}/dpp_registration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dppClientRegistrationDelete(id, callback)</td>
    <td style="padding:15px">Delete registered client.</td>
    <td style="padding:15px">{base_path}/{version}/dpp_registration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCloudauthReadUserPolicy(callback)</td>
    <td style="padding:15px">Fetch policy that allows wireless network access for users.</td>
    <td style="padding:15px">{base_path}/{version}/user_policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCloudauthUpdateUserPolicy(userPolicy, callback)</td>
    <td style="padding:15px">Configure policy to allow wireless network access for users.</td>
    <td style="padding:15px">{base_path}/{version}/user_policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCloudauthDeleteUserPolicy(callback)</td>
    <td style="padding:15px">Delete existing User Policy.</td>
    <td style="padding:15px">{base_path}/{version}/user_policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsGetGroupsV2(limit, offset, callback)</td>
    <td style="padding:15px">Get all groups.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v2/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsCreateGroupV2(group, callback)</td>
    <td style="padding:15px">Create new group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v2/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsCreateGroupV3(group, callback)</td>
    <td style="padding:15px">Create new group with specified properties.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v3/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsCloneGroup(groupAttributes, callback)</td>
    <td style="padding:15px">Clone and create new group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v2/groups/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsGetGroupsTemplateData(groups, callback)</td>
    <td style="padding:15px">Get configuration mode set per device type for groups.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v2/groups/template_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsGetGroupsProperties(groups, callback)</td>
    <td style="padding:15px">Get properties set for groups.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsUpdateGroupPropertiesV2(group, updateAttributes, callback)</td>
    <td style="padding:15px">Update properties for the given group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v2/groups/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsUpdateGroupName(group, newGroupName, callback)</td>
    <td style="padding:15px">Update group name for the given group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/{pathv1}/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsGetCustDefaultGroup(callback)</td>
    <td style="padding:15px">Get default group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/default_group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsSetCustDefaultGroup(groupAttributes, callback)</td>
    <td style="padding:15px">Set default group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/default_group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsDeleteGroup(group, callback)</td>
    <td style="padding:15px">Delete existing group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupForConnectorUsingGET(connectorId, callback)</td>
    <td style="padding:15px">Returns connector to group mapping</td>
    <td style="padding:15px">{base_path}/{version}/v1/iot_gateways/groups/connector/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">moveUsingPOST(request, callback)</td>
    <td style="padding:15px">Group move</td>
    <td style="padding:15px">{base_path}/{version}/v1/iot_gateways/groups/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroupUsingDELETE(request, callback)</td>
    <td style="padding:15px">Group move</td>
    <td style="padding:15px">{base_path}/{version}/v1/iot_gateways/groups/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsMspApiGetMappedTenants(offset, limit, groupName, callback)</td>
    <td style="padding:15px">Get list of customers mapped to MSP group based on limit and offset.</td>
    <td style="padding:15px">{base_path}/{version}/msp_api/v1/groups/{pathv1}/customers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsPreProvisionGroup(data, callback)</td>
    <td style="padding:15px">Pre Provision a group to the device</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/preassign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTemplatesGetTemplates(group, limit, offset, template, deviceType = 'IAP', version, model, q, callback)</td>
    <td style="padding:15px">Get all templates in group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/{pathv1}/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTemplatesCreateTemplate(group, name, deviceType = 'IAP', version, model, template, callback)</td>
    <td style="padding:15px">Create new template.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/{pathv1}/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTemplatesUpdateTemplate(group, name, deviceType = 'IAP', version, model, template, callback)</td>
    <td style="padding:15px">Update existing template.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/{pathv1}/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTemplatesGetTemplate(group, template, callback)</td>
    <td style="padding:15px">Get template text for a template in group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/{pathv1}/templates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTemplatesDeleteTemplate(group, template, callback)</td>
    <td style="padding:15px">Delete existing template.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/{pathv1}/templates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsCreateSnapshotForGroup(group, groupAttributes, callback)</td>
    <td style="padding:15px">Create new configuration backup for group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/snapshot/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsCreateSnapshotsForMultipleGroups(groupAttributes, callback)</td>
    <td style="padding:15px">Create new configuration backup for multiple groups.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/snapshot/backups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsGetSnapshotsForGroup(group, callback)</td>
    <td style="padding:15px">Get all configuration backups for the given group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/{pathv1}/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsUpdateDoNotDelete(group, snapshotAttributes, callback)</td>
    <td style="padding:15px">Update do-not-delete flag for list of configuration backups for the given group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/{pathv1}/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsGetLastRestoreLogsForGroup(group, callback)</td>
    <td style="padding:15px">Get last restore logs for the given group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/{pathv1}/last_restore_log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsGetBackupLogForSnapshot(group, snapshot, callback)</td>
    <td style="padding:15px">Get backup-log for the given configuration backup for the given group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/{pathv1}/snapshots/{pathv2}/backup_log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsGetBackupStatusForSnapshot(group, snapshot, callback)</td>
    <td style="padding:15px">Get status of configuration backup for the given group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/{pathv1}/snapshots/{pathv2}/backup_status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsGetRestoreStatusForSnapshot(group, snapshot, callback)</td>
    <td style="padding:15px">Get status of configuration restore for the given group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/{pathv1}/snapshots/{pathv2}/restore_status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsRestoreSnapshotForGroup(group, snapshot, deviceType = 'IAP', callback)</td>
    <td style="padding:15px">Restore configuration backup of a group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/groups/{pathv1}/snapshots/{pathv2}/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGroupsMoveDevices(data, callback)</td>
    <td style="padding:15px">Move devices to a group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDevicesGetDeviceGroup(deviceSerial, callback)</td>
    <td style="padding:15px">Get group for a device.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/{pathv1}/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDevicesGetDeviceConfiguration(deviceSerial, callback)</td>
    <td style="padding:15px">Get last known running configuration for a device.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/{pathv1}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDevicesGetDeviceConfigurationDetails(deviceSerial, details, callback)</td>
    <td style="padding:15px">Get configuration details for a device (only for template groups).</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/{pathv1}/config_details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDevicesGetDevicesTemplateDetails(deviceSerials, callback)</td>
    <td style="padding:15px">Get templates for a list of devices.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDevicesGetGroupsTemplateDetails(limit, offset, includeGroups, excludeGroups, allGroups, deviceType = 'IAP', callback)</td>
    <td style="padding:15px">Get templates of devices present in the given list of groups.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/groups/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDevicesGetHashTemplateDetails(templateHash, limit, offset, excludeHash, deviceType = 'IAP', callback)</td>
    <td style="padding:15px">Get templates of devices for given template hash (Only allowed for user having all_groups access or</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/{pathv1}/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDevicesGetDeviceVariabilisedTemplate(deviceSerial, callback)</td>
    <td style="padding:15px">Get variablised template for an Aruba Switch.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/{pathv1}/variablised_template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDevicesUpdateSshConnectionInfo(deviceSerial, params, callback)</td>
    <td style="padding:15px">Set Username, password required for establishing SSH connection to switch.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/{pathv1}/ssh_connection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDevicesUsingGET(collectorId, deviceAddress, deviceClasses, deviceType = 'BLE', page, since, size, sort, callback)</td>
    <td style="padding:15px">Return Pageable List of all devices</td>
    <td style="padding:15px">{base_path}/{version}/v1/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDevicesByAttributeUsingGET(attName, attValue, collectorId, page, since, size, sort, callback)</td>
    <td style="padding:15px">Return Pageable List of all devices</td>
    <td style="padding:15px">{base_path}/{version}/v1/devices/attribute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceClassifiedCountUsingGET(collectorId, deviceType = 'BLE', since, callback)</td>
    <td style="padding:15px">Return the count of devices with one or more device classes</td>
    <td style="padding:15px">{base_path}/{version}/v1/devices/statistics/classified/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCountUsingGET(collectorId, since, callback)</td>
    <td style="padding:15px">Return the count of devices</td>
    <td style="padding:15px">{base_path}/{version}/v1/devices/statistics/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceClassifiedStatisticsUsingGET(collectorId, deviceType = 'BLE', since, callback)</td>
    <td style="padding:15px">Return the count of devices by Device Class</td>
    <td style="padding:15px">{base_path}/{version}/v1/devices/statistics/device_classes/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportersCountUsingGET(collectorId, since, callback)</td>
    <td style="padding:15px">Return the count of devices reporters by Collector</td>
    <td style="padding:15px">{base_path}/{version}/v1/devices/statistics/reporters/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCountByTypeUsingGET(collectorId, since, callback)</td>
    <td style="padding:15px">Return the count of devices by type</td>
    <td style="padding:15px">{base_path}/{version}/v1/devices/statistics/types/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCountByCollectorIdUsingGET(aggregation, collectorId, endTime, startTime, type = 'BLE', callback)</td>
    <td style="padding:15px">Return a time-series device count by collectorId</td>
    <td style="padding:15px">{base_path}/{version}/v1/devices/timeseries/count_device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceUsingGET(deviceId, callback)</td>
    <td style="padding:15px">Return a device by Id</td>
    <td style="padding:15px">{base_path}/{version}/v1/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceAttributesUsingGET(deviceId, page, size, sort, callback)</td>
    <td style="padding:15px">Return Pageable List of Attributes</td>
    <td style="padding:15px">{base_path}/{version}/v1/devices/{pathv1}/attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTimeseriesMetricsUsingGET(aggregation, collectorId, deviceId, endTime, metrics = 'RSSI', reporter, startTime, callback)</td>
    <td style="padding:15px">Return the time-series from device</td>
    <td style="padding:15px">{base_path}/{version}/v1/devices/{pathv1}/timeseries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsMspApiGetCustomerDevices(customerId, offset, limit, deviceType = 'iap', callback)</td>
    <td style="padding:15px">Get list of devices and licenses under the Customer account based on limit and offset, offset shoul</td>
    <td style="padding:15px">{base_path}/{version}/msp_api/v1/customers/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsMspApiMoveDevicesToCustomer(customerId, devices, callback)</td>
    <td style="padding:15px">Move a device to an end-customer</td>
    <td style="padding:15px">{base_path}/{version}/msp_api/v1/customers/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsMspApiGetDevices(offset, limit, deviceAllocationStatus, deviceType = 'iap', customerName, callback)</td>
    <td style="padding:15px">Get list of devices and licenses under the MSP account based on limit and offset, offset should be</td>
    <td style="padding:15px">{base_path}/{version}/msp_api/v1/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceNbApiApiDeviceInventoryGetDevices(limit, offset, skuType, callback)</td>
    <td style="padding:15px">Get devices from device inventory</td>
    <td style="padding:15px">{base_path}/{version}/platform/device_inventory/v1/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceNbApiApiDeviceInventoryAddDevice(params, callback)</td>
    <td style="padding:15px">Add device using Mac and Serial number</td>
    <td style="padding:15px">{base_path}/{version}/platform/device_inventory/v1/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceNbApiApiDeviceInventoryDeleteDevice(params, callback)</td>
    <td style="padding:15px">Delete devices using Serial number</td>
    <td style="padding:15px">{base_path}/{version}/platform/device_inventory/v1/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceNbApiApiDeviceInventoryGetDevicesStats(skuType, serviceType, callback)</td>
    <td style="padding:15px">Get devices stats</td>
    <td style="padding:15px">{base_path}/{version}/platform/device_inventory/v1/devices/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceNbApiApiDeviceInventoryVerifyDeviceAddition(params, callback)</td>
    <td style="padding:15px">Verify device addition</td>
    <td style="padding:15px">{base_path}/{version}/platform/device_inventory/v1/devices/verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceNbApiApiDeviceInventoryGetArchiveDevices(limit, offset, callback)</td>
    <td style="padding:15px">Get Archived devices from device inventory</td>
    <td style="padding:15px">{base_path}/{version}/platform/device_inventory/v1/devices/archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceNbApiApiDeviceInventoryArchiveDevices(serialList, callback)</td>
    <td style="padding:15px">Archive devices using Serial list</td>
    <td style="padding:15px">{base_path}/{version}/platform/device_inventory/v1/devices/archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceNbApiApiDeviceInventoryUnarchiveDevices(serialList, callback)</td>
    <td style="padding:15px">Unarchive devices using Serial list</td>
    <td style="padding:15px">{base_path}/{version}/platform/device_inventory/v1/devices/unarchive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDevicesGetDeviceTemplateVariables(deviceSerial, callback)</td>
    <td style="padding:15px">Get template variables for a device.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/{pathv1}/template_variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDevicesCreateDeviceTemplateVariables(deviceSerial, variables, callback)</td>
    <td style="padding:15px">Create template variables for a device.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/{pathv1}/template_variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDevicesUpdateDeviceTemplateVariables(deviceSerial, variables, callback)</td>
    <td style="padding:15px">Update template variables for a device.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/{pathv1}/template_variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDevicesReplaceDeviceTemplateVariables(deviceSerial, variables, callback)</td>
    <td style="padding:15px">Replace all or delete some of the template variables for a device.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/{pathv1}/template_variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDevicesDeleteDeviceTemplateVariables(deviceSerial, callback)</td>
    <td style="padding:15px">Delete all of the template variables for a device.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/{pathv1}/template_variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDevicesGetAllDevicesTemplateVariables(limit, offset, format = 'JSON', callback)</td>
    <td style="padding:15px">Get template variables for all devices, Response is sorted by device serial.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/template_variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDevicesCreateAllDevicesTemplateVariables(format = 'JSON', variables, callback)</td>
    <td style="padding:15px">Create template variables for all devices.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/template_variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDevicesReplaceAllDevicesTemplateVariables(format = 'JSON', variables, callback)</td>
    <td style="padding:15px">Replace all or delete some of the template variables for all devices.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/template_variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDevicesUpdateAllDevicesTemplateVariables(variables, callback)</td>
    <td style="padding:15px">Update template variables for all devices (Only JSON Payload).</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/template_variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDevicesRecoverMdDevice(deviceSerial, callback)</td>
    <td style="padding:15px">Trigger Mobility Device recovery by resetting (delete and add) Device configuration.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/devices/{pathv1}/recover_device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCertificatesGetCertificates(limit, offset, q, callback)</td>
    <td style="padding:15px">Get Certificates details uploaded.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/certificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCertificatesUploadCertificate(certificate, callback)</td>
    <td style="padding:15px">Upload a certificate.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/certificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCertificatesDeleteCertificate(certificate, callback)</td>
    <td style="padding:15px">Delete existing certificate.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/certificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCertificatesMspUpdateCertificate(certificate, callback)</td>
    <td style="padding:15px">Update a certificate.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/msp/certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCpLogoGetCpLogos(limit, offset, callback)</td>
    <td style="padding:15px">Get Captive Portal Logos uploaded.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/cplogo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCpLogoUploadCpLogo(cplogo, callback)</td>
    <td style="padding:15px">Upload a captive portal logo.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/cplogo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCpLogoDeleteCpLogo(checksum, callback)</td>
    <td style="padding:15px">Delete existing captive portal logo.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/cplogo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTemplatesGetMspCustomerTemplates(limit, offset, deviceType = 'IAP', version, model, callback)</td>
    <td style="padding:15px">Get MSP customer level template details.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/msp/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTemplatesGetMspCustomerTemplateText(deviceType = 'IAP', version, model, callback)</td>
    <td style="padding:15px">Get MSP customer level template text.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/msp/templates/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTemplatesDeleteMspCustomerTemplate(deviceType = 'IAP', version, model, callback)</td>
    <td style="padding:15px">Delete MSP customer template.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/msp/templates/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTemplatesSetMspCustomerTemplate(deviceType = 'IAP', version, model, template, callback)</td>
    <td style="padding:15px">Update MSP customer level template.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v2/msp/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTemplatesGetEndCustomerTemplates(cid, limit, offset, deviceType = 'IAP', version, model, callback)</td>
    <td style="padding:15px">Get end customer level template details.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/msp/templates/customer/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTemplatesGetEndCustomerTemplateText(cid, deviceType = 'IAP', version, model, callback)</td>
    <td style="padding:15px">Get end customer level template text.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/msp/templates/customer/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTemplatesDeleteEndCustomerTemplate(cid, deviceType = 'IAP', version, model, callback)</td>
    <td style="padding:15px">Delete end customer template.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/msp/templates/customer/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTemplatesSetEndCustomerTemplate(cid, deviceType = 'IAP', version, model, template, callback)</td>
    <td style="padding:15px">Update end customer level template.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v2/msp/templates/customer/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTemplatesGetMspTmplDifferCustsGroups(deviceType = 'IAP', version, model, limit, offset, callback)</td>
    <td style="padding:15px">Get customers and groups where given MSP level template is not applied.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/msp/templates/differences/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTemplatesGetMspTmplEndCustDifferGroups(cid, deviceType = 'IAP', version, model, callback)</td>
    <td style="padding:15px">Get groups for given end customer where MSP Level template is not applied.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/msp/templates/differences/customer/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTemplatesApplyMspCustomerTemplate(applyCustomers, deviceType = 'IAP', version, model, callback)</td>
    <td style="padding:15px">Apply MSP customer level template to end customers.

This would not apply template to template grou</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v2/msp/templates/end_customers/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTemplatesApplyEndCustomerTemplate(cid, applyGroups, deviceType = 'IAP', version, model, callback)</td>
    <td style="padding:15px">Apply end customer template to template groups at end customer.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v2/msp/templates/end_customers/{pathv1}/{pathv2}/{pathv3}/{pathv4}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiConfigModeGetCustConfigMode(callback)</td>
    <td style="padding:15px">Get configuration mode as either Monitor or Managed mode at customer level.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/mode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiConfigModeSetCustConfigMode(configMode, callback)</td>
    <td style="padding:15px">Set configuration mode as either Monitor or Manage at customer level.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/mode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiConfigModeGetGroupConfigMode(limit, offset, q, callback)</td>
    <td style="padding:15px">Get configuration mode for devices as either Monitor or Managed mode at group level.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/mode/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiConfigModeSetGroupConfigMode(configMode, callback)</td>
    <td style="padding:15px">Set configuration mode as either Monitor or Manage at group level.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/mode/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiConfigModeGetDeviceConfigMode(limit, offset, group, callback)</td>
    <td style="padding:15px">Get configuration mode as either Monitor or Managed mode at device level.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/mode/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiConfigModeSetDeviceConfigMode(configMode, callback)</td>
    <td style="padding:15px">Set configuration mode as either Monitor or Manage for given devices.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/mode/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiConfigModeGetDeviceSerialsConfigMode(deviceSerials, callback)</td>
    <td style="padding:15px">Get configuration mode as either Monitor or Managed mode for device serials.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/mode/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiVariablesFirewallGetVfwGroups(callback)</td>
    <td style="padding:15px">Get whitelisted groups in Variables Firewall.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/variables_firewall/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiVariablesFirewallUpdateVfwGroups(groups, callback)</td>
    <td style="padding:15px">Add groups to Variables Firewall whitelist.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/variables_firewall/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiVariablesFirewallDeleteVfwGroup(group, callback)</td>
    <td style="padding:15px">Delete group from Variables Firewall whitelist.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/variables_firewall/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiVariablesFirewallGetVfwVariables(callback)</td>
    <td style="padding:15px">Get whitelisted variables in Variables Firewall.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/variables_firewall/variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiVariablesFirewallUpdateVfwVariables(variables, callback)</td>
    <td style="padding:15px">Add variables to Variables Firewall whitelist.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/variables_firewall/variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiVariablesFirewallDeleteVfwVariable(variable, callback)</td>
    <td style="padding:15px">Delete variable from Variables Firewall whitelist.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/variables_firewall/variables/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiNbUiGroupConfigSetGroupConfigCountryCode(setGroupConfigCountryCode, callback)</td>
    <td style="padding:15px">Set country code at group level (For UI groups only, not supported for template groups).

Note: IAP</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/country?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiNbUiGroupConfigGetGroupCountry(group, callback)</td>
    <td style="padding:15px">Get country code set for group (For UI groups only, not supported for template groups).</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/{pathv1}/country?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSaveCommitGetGroupsAutoCommitState(limit, offset, q, callback)</td>
    <td style="padding:15px">Get auto commit state as either On or Off at group level.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/auto_commit_state/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSaveCommitSetGroupsAutoCommitState(autoCommitState, callback)</td>
    <td style="padding:15px">Set auto commit state as either On or Off at group level.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/auto_commit_state/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSaveCommitGetDeviceSerialsAutoCommitState(deviceSerials, callback)</td>
    <td style="padding:15px">Get auto commit state as either On or Off for device serials.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/auto_commit_state/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSaveCommitSetDeviceSerialsAutoCommitState(autoCommitState, callback)</td>
    <td style="padding:15px">Set auto commit state as either On or Off for given devices.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/auto_commit_state/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSaveCommitCommitGroupConfig(commitMode, callback)</td>
    <td style="padding:15px">Commit configurations for given groups.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/commit/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSaveCommitCommitDeviceConfig(commitMode, callback)</td>
    <td style="padding:15px">Commit configurations for given devices.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/commit/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiBlacklistClientGetBlacklistClients(deviceId, callback)</td>
    <td style="padding:15px">Get all denylist client mac address in device.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/swarm/{pathv1}/blacklisting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiBlacklistClientAddBlacklistClients(deviceId, blacklist, callback)</td>
    <td style="padding:15px">Add denylist clients</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/swarm/{pathv1}/blacklisting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiBlacklistClientDeleteBlacklistClients(deviceId, blacklist, callback)</td>
    <td style="padding:15px">Delete denylist clients</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/swarm/{pathv1}/blacklisting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiWlanGetWlanList(groupNameOrGuid, callback)</td>
    <td style="padding:15px">Get WLAN list of an UI group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/wlan/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiWlanGetWlanTemplate(groupNameOrGuid, callback)</td>
    <td style="padding:15px">Get WLAN default configuration.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/wlan/{pathv1}/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiWlanGetProtocolMap(groupNameOrGuid, callback)</td>
    <td style="padding:15px">Get WLAN access rule protocol map.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/wlan/{pathv1}/protocol_map?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiWlanGetAccessRuleServices(groupNameOrGuid, callback)</td>
    <td style="padding:15px">Get WLAN access rule services.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/wlan/{pathv1}/access_rule_services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiWlanDeleteWlan(groupNameOrGuid, wlanName, callback)</td>
    <td style="padding:15px">Delete an existing WLAN.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/wlan/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiWlanGetWlanV2(groupNameOrGuid, wlanName, callback)</td>
    <td style="padding:15px">Get the information of an existing WLAN.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v2/wlan/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiWlanCreateWlanV2(groupNameOrGuid, wlanName, wlanData, callback)</td>
    <td style="padding:15px">Create a new WLAN.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v2/wlan/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiWlanCleanUpAndUpdateWlanV2(groupNameOrGuid, wlanName, wlanData, callback)</td>
    <td style="padding:15px">Update an existing WLAN and clean up unsupported fields.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v2/wlan/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiWlanUpdateWlanV2(groupNameOrGuid, wlanName, wlanData, callback)</td>
    <td style="padding:15px">Update an existing WLAN.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v2/wlan/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFullWlanGetWlanList(groupNameOrGuid, callback)</td>
    <td style="padding:15px">Get WLAN list of an UI group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/full_wlan/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFullWlanGetWlanTemplate(groupNameOrGuid, callback)</td>
    <td style="padding:15px">Get WLAN default configuration.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/full_wlan/{pathv1}/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFullWlanGetProtocolMap(groupNameOrGuid, callback)</td>
    <td style="padding:15px">Get WLAN access rule protocol map.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/full_wlan/{pathv1}/protocol_map?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFullWlanGetAccessRuleServices(groupNameOrGuid, callback)</td>
    <td style="padding:15px">Get WLAN access rule services.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/full_wlan/{pathv1}/access_rule_services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFullWlanGetWlan(groupNameOrGuid, wlanName, callback)</td>
    <td style="padding:15px">Get the information of an existing WLAN.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/full_wlan/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFullWlanCreateWlan(groupNameOrGuid, wlanName, wlanData, callback)</td>
    <td style="padding:15px">Create a new WLAN.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/full_wlan/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFullWlanUpdateWlan(groupNameOrGuid, wlanName, wlanData, callback)</td>
    <td style="padding:15px">Update an existing WLAN.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/full_wlan/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFullWlanDeleteWlan(groupNameOrGuid, wlanName, callback)</td>
    <td style="padding:15px">Delete an existing WLAN.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/full_wlan/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFullHotspotGetHotspotList(groupNameOrGuid, callback)</td>
    <td style="padding:15px">Get hotspot list of an UI group or swarm.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/full_hotspot/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFullHotspotGetHotspotListByModeName(groupNameOrGuid, modeName, callback)</td>
    <td style="padding:15px">Get hotspot list of an UI group or swarm with mode name</td>
    <td style="padding:15px">{base_path}/{version}/configuration/full_hotspot/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFullHotspotGetHotspotTemplates(groupNameOrGuid, callback)</td>
    <td style="padding:15px">Get hotspot default configuration.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/full_hotspot/{pathv1}/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFullHotspotGetHotspot(groupNameOrGuid, hotspotName, modeName, callback)</td>
    <td style="padding:15px">Get the information of an existing hotspot.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/full_hotspot/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFullHotspotCreateHotspot(groupNameOrGuid, hotspotName, modeName, hotspotData, callback)</td>
    <td style="padding:15px">Create a new hotspot.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/full_hotspot/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFullHotspotUpdateHotspot(groupNameOrGuid, hotspotName, modeName, hotspotData, callback)</td>
    <td style="padding:15px">Update an existing hotspot.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/full_hotspot/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFullHotspotDeleteHotspot(groupNameOrGuid, hotspotName, modeName, callback)</td>
    <td style="padding:15px">Delete an existing hotspot.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/full_hotspot/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiApClisGetConfigurationClis(groupNameOrGuid, version, callback)</td>
    <td style="padding:15px">Get AP configuration.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/ap_cli/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiApClisUpdateConfigurationClis(groupNameOrGuid, clis, callback)</td>
    <td style="padding:15px">Replace AP configuration.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/ap_cli/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiApClisGetApSettingsClis(serialNumber, callback)</td>
    <td style="padding:15px">Get per AP setting.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/ap_settings_cli/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiApClisUpdateApSettingsClis(serialNumber, clis, callback)</td>
    <td style="padding:15px">Replace per AP setting.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/ap_settings_cli/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiApClisGetSwarmVariables(groupNameOrGuid, callback)</td>
    <td style="padding:15px">Get variables config.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/iap_variables/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiApClisUpdateSwarmVariables(groupNameOrGuid, variables, callback)</td>
    <td style="padding:15px">Replace AP variables.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/iap_variables/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiApClisGetDirtyDiff(groupNameOrGuid, limit, offset, callback)</td>
    <td style="padding:15px">Get dirty diff.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/dirty_diff/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSystemConfigGetSystemConfig(groupNameOrGuid, callback)</td>
    <td style="padding:15px">Get System Config.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/system_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSystemConfigUpdateSystemConfig(groupNameOrGuid, systemConfigInfo, callback)</td>
    <td style="padding:15px">Update system config.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/system_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRfConfigGetArmConfig(groupNameOrGuid, callback)</td>
    <td style="padding:15px">Get ARM configuration.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/arm/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRfConfigUpdateArmConfig(groupNameOrGuid, armInfo, callback)</td>
    <td style="padding:15px">Update ARM configuration.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/arm/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRfConfigGetAllDot11gRadioProfile(groupNameOrGuid, callback)</td>
    <td style="padding:15px">Get all Dot11g Radio Profiles.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/dot11g_radio_profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRfConfigGetDot11gRadioConfigByName(groupNameOrGuid, name, callback)</td>
    <td style="padding:15px">Get Dot11g radio profile.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/dot11g_radio_profile/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRfConfigUpdateDot11gRadioProfile(groupNameOrGuid, name, dot11gRadioProfile, callback)</td>
    <td style="padding:15px">Update/Create Dot11g radio profile.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/dot11g_radio_profile/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRfConfigDeleteDot11gRadioProfile(groupNameOrGuid, name, callback)</td>
    <td style="padding:15px">Delete Dot11g radio profile.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/dot11g_radio_profile/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRfConfigGetAllDot11aRadioProfile(groupNameOrGuid, callback)</td>
    <td style="padding:15px">Get all Dot11a Radio Profiles.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/dot11a_radio_profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRfConfigGetDot11aRadioConfigByName(groupNameOrGuid, name, callback)</td>
    <td style="padding:15px">Get Dot11a radio profile.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/dot11a_radio_profile/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRfConfigUpdateDot11aRadioProfile(groupNameOrGuid, name, dot11aRadioProfile, callback)</td>
    <td style="padding:15px">Update/Create Dot11a radio profile.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/dot11a_radio_profile/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRfConfigDeleteDot11aRadioProfile(groupNameOrGuid, name, callback)</td>
    <td style="padding:15px">Delete an existing Dot11a radio profile.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/dot11a_radio_profile/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiApSshCredentialGetGroupSshCredential(groupName, callback)</td>
    <td style="padding:15px">Get ssh credential in group level.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/group/ssh_credential/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiApSshCredentialUpdateGroupSshCredential(groupName, credentialInfo, callback)</td>
    <td style="padding:15px">Update ssh credential in group level.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/group/ssh_credential/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiApSshCredentialGetDeviceSshCredential(serialNumberOrGuid, callback)</td>
    <td style="padding:15px">Get ssh credential in device level.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/device/ssh_credential/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiApSshCredentialUpdateDeviceSshCredential(serialNumberOrGuid, credentialInfo, callback)</td>
    <td style="padding:15px">Update ssh credential in device level.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/device/ssh_credential/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiApSettingsGetApSettingsV2(serialNumber, callback)</td>
    <td style="padding:15px">Get an existing ap settings.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v2/ap_settings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiApSettingsUpdateApSettingsV2(serialNumber, apSettingsData, callback)</td>
    <td style="padding:15px">Update an existing ap settings.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v2/ap_settings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSwarmConfigGetSwarmConfigV2(guid, callback)</td>
    <td style="padding:15px">Get an existing swarm config.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v2/swarm_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiSwarmConfigUpdateSwarmConfigV2(guid, swarmConfigData, callback)</td>
    <td style="padding:15px">Update an existing swarm config.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v2/swarm_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCxConfigGetInterfaces(deviceSerial, groupName, callback)</td>
    <td style="padding:15px">Get Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/switch/cx/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCxConfigUpdateInterfaces(deviceSerial, groupName, body, callback)</td>
    <td style="padding:15px">Update Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/switch/cx/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCxConfigGetLag(deviceSerial, groupName, callback)</td>
    <td style="padding:15px">Get LAGs</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/switch/cx/lags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCxConfigCrudLag(deviceSerial, groupName, body, callback)</td>
    <td style="padding:15px">Create/Update/Delete LAGs</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/switch/cx/lags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCxConfigGetLoopPrevention(deviceSerial, groupName, callback)</td>
    <td style="padding:15px">Get Loop Prevention</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/switch/cx/loop-prevention?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCxConfigUpdateLoopPrevention(deviceSerial, groupName, body, callback)</td>
    <td style="padding:15px">Update Loop Prevention</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/switch/cx/loop-prevention?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCxConfigGetProperties(deviceSerial, groupName, callback)</td>
    <td style="padding:15px">Get Properties</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/switch/cx/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCxConfigUpdateProperties(deviceSerial, groupName, body, callback)</td>
    <td style="padding:15px">Update Properties</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/switch/cx/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCxConfigGetSyslog(deviceSerial, groupName, callback)</td>
    <td style="padding:15px">Get Syslog</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/switch/cx/syslog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCxConfigCrudSyslog(deviceSerial, groupName, body, callback)</td>
    <td style="padding:15px">Create/Update/Delete Syslog</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/switch/cx/syslog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCxConfigGetVlans(deviceSerial, groupName, callback)</td>
    <td style="padding:15px">Get VLANs</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/switch/cx/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCxConfigCrudVlans(deviceSerial, groupName, body, callback)</td>
    <td style="padding:15px">Create/Update/Delete VLANs</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/switch/cx/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCxConfigGetAuthentication(deviceSerial, groupName, callback)</td>
    <td style="padding:15px">Get Port Access Authentication</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/switch/cx/port-access-auth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCxConfigUpdateAuthentication(deviceSerial, groupName, body, callback)</td>
    <td style="padding:15px">Update Port Access Authentication</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/switch/cx/port-access-auth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHppcSwitchGetGroupPorts(groupName, callback)</td>
    <td style="padding:15px">Get ports name for a group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/aos_switch/ports/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHppcSwitchSetGroupPorts(groupName, ports, callback)</td>
    <td style="padding:15px">Update ports name for a group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/aos_switch/ports/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHppcSwitchGetDevicePorts(deviceSerial, callback)</td>
    <td style="padding:15px">Get ports name for a device.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/aos_switch/ports/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHppcSwitchSetDevicePorts(deviceSerial, ports, callback)</td>
    <td style="padding:15px">Update ports name for a device.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/aos_switch/ports/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHppcSwitchGetGroupVlans(groupName, callback)</td>
    <td style="padding:15px">Get vlans with tagged, untagged and isolated ports for a group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/aos_switch/vlans/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHppcSwitchSetGroupVlans(groupName, vlans, callback)</td>
    <td style="padding:15px">Update vlans with tagged, untagged and isolated ports for a group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/aos_switch/vlans/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHppcSwitchGetDeviceVlans(deviceSerial, callback)</td>
    <td style="padding:15px">Get vlans with tagged, untagged and isolated ports for a device.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/aos_switch/vlans/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHppcSwitchSetDeviceVlans(deviceSerial, vlans, callback)</td>
    <td style="padding:15px">Update vlans with tagged, untagged and isolated ports for a device.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/aos_switch/vlans/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHppcSwitchGetGroupAdmin(groupName, callback)</td>
    <td style="padding:15px">Get admin SSH details of a group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/aos_switch/system/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHppcSwitchSetGroupAdmin(groupName, adminDetails, callback)</td>
    <td style="padding:15px">Update admin SSH details of a group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/aos_switch/system/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHppcSwitchGetDeviceAdmin(deviceSerial, callback)</td>
    <td style="padding:15px">Get admin SSH details of a device.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/aos_switch/system/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHppcSwitchSetDeviceAdmin(deviceSerial, adminDetails, callback)</td>
    <td style="padding:15px">Update admin SSH details of a device.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/aos_switch/system/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHppcSwitchGetGroupSystemTime(groupName, callback)</td>
    <td style="padding:15px">Get system time details for a group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/aos_switch/system_time/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHppcSwitchSetGroupSystemTime(groupName, dayLightTimeRule, callback)</td>
    <td style="padding:15px">Update system time details for a group.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/aos_switch/system_time/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHppcSwitchGetDeviceSystemTime(deviceSerial, callback)</td>
    <td style="padding:15px">Get system time details for a device.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/aos_switch/system_time/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiHppcSwitchSetDeviceSystemTime(deviceSerial, dayLightTimeRule, callback)</td>
    <td style="padding:15px">Update system time details for a device.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/v1/aos_switch/system_time/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiActionCommandSendCommandToDevice(serial, command, callback)</td>
    <td style="padding:15px">Generic commands for device</td>
    <td style="padding:15px">{base_path}/{version}/device_management/v1/device/{pathv1}/action/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiActionCommandSendMultiLineCmd(serial, command, port, callback)</td>
    <td style="padding:15px">Generic Action Command for bouncing interface or POE (power-over-ethernet) port</td>
    <td style="padding:15px">{base_path}/{version}/device_management/v1/device/{pathv1}/action/{pathv2}/port/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiActionCommandSendMultiLineCmdV2(serial, command, body, callback)</td>
    <td style="padding:15px">Generic Action Command for bouncing interface or POE (power-over-ethernet) port</td>
    <td style="padding:15px">{base_path}/{version}/device_management/v2/device/{pathv1}/action/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiActionCommandSendCommandToSwarm(swarmId, command, callback)</td>
    <td style="padding:15px">Generic commands for swarm</td>
    <td style="padding:15px">{base_path}/{version}/device_management/v1/swarm/{pathv1}/action/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiActionCommandSendDisconnectUser(serial, body, callback)</td>
    <td style="padding:15px">Disconnect User</td>
    <td style="padding:15px">{base_path}/{version}/device_management/v1/device/{pathv1}/action/disconnect_user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiActionCommandSendSpeedTest(serial, body, callback)</td>
    <td style="padding:15px">Speed Test</td>
    <td style="padding:15px">{base_path}/{version}/device_management/v1/device/{pathv1}/action/speedtest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiActionCommandGetCommandStatus(taskId, callback)</td>
    <td style="padding:15px">Status</td>
    <td style="padding:15px">{base_path}/{version}/device_management/v1/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiActivateActivateApiMoveDevices(params, callback)</td>
    <td style="padding:15px">Move the devices across customers</td>
    <td style="padding:15px">{base_path}/{version}/activate/v1/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiActionCommandSendEnrollPkiCertificateSwitch(serial, body, callback)</td>
    <td style="padding:15px">Action command for enroll est certificate</td>
    <td style="padding:15px">{base_path}/{version}/tools/action_cmd/device/{pathv1}/enroll_est_cert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiMmDeviceActivateSync(mmName, callback)</td>
    <td style="padding:15px">Trigger activate sync for given MobilityMaster</td>
    <td style="padding:15px">{base_path}/{version}/device_management/v1/mobility_master/{pathv1}/activate_sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiMmDeviceStaticMdMmAssign(deviceSerial, mmName, callback)</td>
    <td style="padding:15px">Statically assign Mobility Master to Mobility Device.</td>
    <td style="padding:15px">{base_path}/{version}/device_management/v1/mobility_master/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiMmDeviceGetMdMmMapping(deviceSerial, callback)</td>
    <td style="padding:15px">Get assigned Mobility Master to Mobility Device.</td>
    <td style="padding:15px">{base_path}/{version}/device_management/v1/mobility_master/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiMmDeviceDelMdMmMapping(deviceSerial, callback)</td>
    <td style="padding:15px">Delete Mobility Master to Mobility Device mapping</td>
    <td style="padding:15px">{base_path}/{version}/device_management/v1/mobility_master/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDispatcherExternalApiGetWebhooksApi(callback)</td>
    <td style="padding:15px">List webhooks</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/webhooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDispatcherExternalApiAddWebhookApi(body, callback)</td>
    <td style="padding:15px">Add Webhook</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/webhooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDispatcherExternalApiGetWebhookItemApi(wid, callback)</td>
    <td style="padding:15px">Webhook setting for a specific item</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/webhooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDispatcherExternalApiDeleteWebhookApi(wid, callback)</td>
    <td style="padding:15px">Delete Webhooks</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/webhooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDispatcherExternalApiUpdateWebhookApi(wid, body, callback)</td>
    <td style="padding:15px">Update webhook settings</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/webhooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDispatcherExternalApiGetWebhookTokenApi(wid, callback)</td>
    <td style="padding:15px">Get Webhook Token</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/webhooks/{pathv1}/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDispatcherExternalApiRefreshWebhookTokenApi(wid, callback)</td>
    <td style="padding:15px">Refresh the webhook token</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/webhooks/{pathv1}/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDispatcherExternalApiTestWebhook(wid, callback)</td>
    <td style="padding:15px">Test for webhook notification</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/webhooks/{pathv1}/ping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDpsMonitoringEndpointsDpsMonitoringApigwEndpointsGetDpsPolicyStats(clusterId, policyName, callback)</td>
    <td style="padding:15px">Gets DPS Policy stats for a given BOC.</td>
    <td style="padding:15px">{base_path}/{version}/datapoints/v1/cluster/{pathv1}/sdwan_policy/policy_stats/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDpsMonitoringEndpointsDpsMonitoringApigwEndpointsGetDpsPolicyStatsHigherWindow(clusterId, policyName, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">Gets DPS Policy stats for a given BOC.</td>
    <td style="padding:15px">{base_path}/{version}/datapoints/v1/cluster/{pathv1}/sdwan_policy/policy_stats_higher_window/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDpsMonitoringEndpointsDpsMonitoringApigwEndpointsGetDpsPoliciesKpiStats(clusterId, callback)</td>
    <td style="padding:15px">DPS Key Performance Indicator for a given BOC.</td>
    <td style="padding:15px">{base_path}/{version}/datapoints/v1/cluster/{pathv1}/sdwan_policies/kpi?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDpsMonitoringEndpointsDpsMonitoringApigwEndpointsGetDpsPoliciesCompliancePercentage(clusterId, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">DPS Compliance percentage of all DPS Policies for a given BOC.</td>
    <td style="padding:15px">{base_path}/{version}/datapoints/v1/cluster/{pathv1}/sdwan_policies/compliance_percentage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDpsMonitoringEndpointsDpsMonitoringApigwEndpointsGetDpsPoliciesStatus(clusterId, callback)</td>
    <td style="padding:15px">DPS Compliance Status of all DPS Policies for a given BOC.</td>
    <td style="padding:15px">{base_path}/{version}/datapoints/v1/cluster/{pathv1}/sdwan_policies/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDpsMonitoringEndpointsDpsMonitoringApigwEndpointsGetDpsPoliciesEventLogs(clusterId, policyName, callback)</td>
    <td style="padding:15px">Gets DPS Policy Event Logs for a given BOC.</td>
    <td style="padding:15px">{base_path}/{version}/datapoints/v1/cluster/{pathv1}/sdwan_policy/event_logs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiDpsSiteMonitoringEndpointsDpsSiteMonitoringApigwEndpointGetDpsSitePolicyStats(siteName, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">Gets DPS Compliance stats for a given Site.</td>
    <td style="padding:15px">{base_path}/{version}/datapoints/v1/sdwan_site/site_policy_stats/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareGetSwarmsFirmwareDetails(group, limit, offset, callback)</td>
    <td style="padding:15px">List Firmware Details of Swarms</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/swarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareGetSwarmFirmwareDetails(swarmId, callback)</td>
    <td style="padding:15px">Firmware Details of Swarm</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/swarms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareGetDevicesFirmwareDetails(group, limit, offset, deviceType, callback)</td>
    <td style="padding:15px">List Firmware Details of Devices</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareGetDeviceFirmwareDetails(serial, callback)</td>
    <td style="padding:15px">Firmware Details of Device</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareGetFirmwareVersionList(deviceType, swarmId, serial, callback)</td>
    <td style="padding:15px">List Firmware Version</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareIsImageAvailable(deviceType, firmwareVersion, callback)</td>
    <td style="padding:15px">Firmware Version</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/versions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareGetFirmwareStatus(swarmId, serial, callback)</td>
    <td style="padding:15px">Firmware Status</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareUpgradeFirmware(body, callback)</td>
    <td style="padding:15px">Firmware Upgrade</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareCancelUpgrade(body, callback)</td>
    <td style="padding:15px">Cancel Scheduled Upgrade</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/upgrade/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareSetFirmwareComplianceCustomer(body, callback)</td>
    <td style="padding:15px">Set Firmware Compliance Version Customer</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v2/upgrade/compliance_version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareSetFirmwareCompliance(body, callback)</td>
    <td style="padding:15px">Set Firmware Compliance Version</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/upgrade/compliance_version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareGetFirmwareCompliance(group, deviceType, callback)</td>
    <td style="padding:15px">Get Firmware Compliance Version</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/upgrade/compliance_version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareDeleteFirmwareCompliance(group, deviceType, callback)</td>
    <td style="padding:15px">Clear Firmware Compliance Version</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/upgrade/compliance_version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareUpgradeFirmwareMsp(body, callback)</td>
    <td style="padding:15px">Firmware Upgrade at MSP Level</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/msp/upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareUpgradeFirmwareCustomer(customerId, body, callback)</td>
    <td style="padding:15px">Firmware Upgrade at Customer Level</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/msp/upgrade/customers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareCancelUpgradeMspV2(body, callback)</td>
    <td style="padding:15px">Cancel Scheduled Upgrade at MSP Level</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v2/msp/upgrade/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareCancelUpgradeCustomerV2(customerId, body, callback)</td>
    <td style="padding:15px">Cancel Scheduled Upgrade at Customer Level</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v2/msp/upgrade/customers/{pathv1}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareGetFirmwareModelFamiliesList(serial, deviceType, callback)</td>
    <td style="padding:15px">List Model Family</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/models?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareSetFirmwareComplianceMsp(body, callback)</td>
    <td style="padding:15px">Set Firmware Compliance Version for MSP customer</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/msp/upgrade/compliance_version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareGetFirmwareComplianceMsp(deviceType, callback)</td>
    <td style="padding:15px">Get Firmware Compliance Version for MSP Customer</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/msp/upgrade/compliance_version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareDeleteFirmwareComplianceMsp(body, callback)</td>
    <td style="padding:15px">Clear Firmware Compliance Version for MSP Customer</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/msp/upgrade/compliance_version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareSetFirmwareComplianceMspTenant(customerId, body, callback)</td>
    <td style="padding:15px">Set Firmware Compliance Version for Tenant</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/msp/upgrade/customers/{pathv1}/compliance_version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareGetFirmwareComplianceMspTenant(customerId, group, deviceType, callback)</td>
    <td style="padding:15px">Get Firmware Compliance Version for Tenant</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/msp/upgrade/customers/{pathv1}/compliance_version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareDeleteFirmwareComplianceMspTenant(customerId, group, deviceType, callback)</td>
    <td style="padding:15px">Clear Firmware Compliance Version for Tenant</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/msp/upgrade/customers/{pathv1}/compliance_version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiFirmwareGetTenantsDetails(deviceType, tenantId, callback)</td>
    <td style="padding:15px">List Tenants of an MSP customer</td>
    <td style="padding:15px">{base_path}/{version}/firmware/v1/msp/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGdprExternalApiGetGdprsApi(callback)</td>
    <td style="padding:15px">List gdprs opt out MAC clients for this customer</td>
    <td style="padding:15px">{base_path}/{version}/gdpr/v1/opt_out_clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGdprExternalApiAddGdprApi(body, callback)</td>
    <td style="padding:15px">Add Gdpr opt out client</td>
    <td style="padding:15px">{base_path}/{version}/gdpr/v1/opt_out_clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGdprGetMac(mac, callback)</td>
    <td style="padding:15px">GDPR Opt out MAC</td>
    <td style="padding:15px">{base_path}/{version}/gdpr/v1/opt_out_clients/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiGdprExternalApiDeleteGdprApi(mac, callback)</td>
    <td style="padding:15px">Delete Opt out Mac</td>
    <td style="padding:15px">{base_path}/{version}/gdpr/v1/opt_out_clients/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiGetApiPortals(sort = '+name', offset, limit, callback)</td>
    <td style="padding:15px">Get all portals with limited data</td>
    <td style="padding:15px">{base_path}/{version}/guest/v1/portals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiCreateApiPortal(portal, callback)</td>
    <td style="padding:15px">Create a new guest portal profile</td>
    <td style="padding:15px">{base_path}/{version}/guest/v1/portals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiPreviewApiPortal(portalId, callback)</td>
    <td style="padding:15px">Get preview url of guest portal profile</td>
    <td style="padding:15px">{base_path}/{version}/guest/v1/portals/preview/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiGetApiPortal(portalId, callback)</td>
    <td style="padding:15px">Get guest portal profile</td>
    <td style="padding:15px">{base_path}/{version}/guest/v1/portals/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiUpdateApiPortal(portalId, portal, callback)</td>
    <td style="padding:15px">Update guest portal profile</td>
    <td style="padding:15px">{base_path}/{version}/guest/v1/portals/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiDeleteApiPortal(portalId, callback)</td>
    <td style="padding:15px">Delete guest portal profile</td>
    <td style="padding:15px">{base_path}/{version}/guest/v1/portals/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiGetApiVisitors(sort = '+name', filterBy = 'name', filterValue, offset, limit, portalId, callback)</td>
    <td style="padding:15px">Get all visitors created against a portal</td>
    <td style="padding:15px">{base_path}/{version}/guest/v1/portals/{pathv1}/visitors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiCreateApiVisitor(portalId, visitor, callback)</td>
    <td style="padding:15px">Create a new guest visitor of a portal</td>
    <td style="padding:15px">{base_path}/{version}/guest/v1/portals/{pathv1}/visitors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiGetApiVisitor(portalId, visitorId, callback)</td>
    <td style="padding:15px">Get guest visitor account</td>
    <td style="padding:15px">{base_path}/{version}/guest/v1/portals/{pathv1}/visitors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiUpdateApiVisitor(portalId, visitorId, visitor, callback)</td>
    <td style="padding:15px">Update guest visitor account</td>
    <td style="padding:15px">{base_path}/{version}/guest/v1/portals/{pathv1}/visitors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiDeleteApiVisitor(portalId, visitorId, callback)</td>
    <td style="padding:15px">Delete guest visitor account</td>
    <td style="padding:15px">{base_path}/{version}/guest/v1/portals/{pathv1}/visitors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiGetApiSessions(sort = '+account_name', essidName, ssidName, filterBy = 'account_name', filterValue, offset, limit, portalId, callback)</td>
    <td style="padding:15px">Get all sessions of a ssid</td>
    <td style="padding:15px">{base_path}/{version}/guest/v1/portals/{pathv1}/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiGetApiGuestWlans(callback)</td>
    <td style="padding:15px">Get all guest wlans</td>
    <td style="padding:15px">{base_path}/{version}/guest/v1/wlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiGetApiGuestEnabled(callback)</td>
    <td style="padding:15px">Check if guest is enabled for current user.</td>
    <td style="padding:15px">{base_path}/{version}/guest/v1/enabled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiGetApiGuestReProvision(callback)</td>
    <td style="padding:15px">Provision cloud guest for current customer</td>
    <td style="padding:15px">{base_path}/{version}/guest/v1/reprovision?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiWifi4euStatus(networkId, langCode, callback)</td>
    <td style="padding:15px">WiFi4EU Status</td>
    <td style="padding:15px">{base_path}/{version}/guest/v1/wifi4eu/lang_code/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiInstallManagerExternalApiInviteInstaller(body, callback)</td>
    <td style="padding:15px">Invite a new installer</td>
    <td style="padding:15px">{base_path}/{version}/v1/invite_installer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiInstallManagerExternalApiAssignGroupToDeviceTypesInSites(body, callback)</td>
    <td style="padding:15px">For a given set of site names, assign Group Names for each device type</td>
    <td style="padding:15px">{base_path}/{version}/v1/assign_group_to_device_types_in_sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchAppsUsingGET(collectorId, keywords, page, size, callback)</td>
    <td style="padding:15px">Search apps</td>
    <td style="padding:15px">{base_path}/{version}/v1/appstore/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findAllInstalledAppsUsingGET(collectorId, page, size, sort, callback)</td>
    <td style="padding:15px">Find all installed apps</td>
    <td style="padding:15px">{base_path}/{version}/v1/appstore/apps/installed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countAllInstalledAppsUsingGET(collectorId, callback)</td>
    <td style="padding:15px">Count all installed apps</td>
    <td style="padding:15px">{base_path}/{version}/v1/appstore/apps/installed/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppUsingGET(appId, collectorId, latestVersion, callback)</td>
    <td style="padding:15px">Get App</td>
    <td style="padding:15px">{base_path}/{version}/v1/appstore/apps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppIconUsingGET(appId, callback)</td>
    <td style="padding:15px">Get App Icon</td>
    <td style="padding:15px">{base_path}/{version}/v1/appstore/apps/{pathv1}/icon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">installAppUsingPOST(appId, request, callback)</td>
    <td style="padding:15px">Install app</td>
    <td style="padding:15px">{base_path}/{version}/v1/appstore/apps/{pathv1}/install?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editInstalledAppUsingPOST(appId, request, callback)</td>
    <td style="padding:15px">Edit Installed app configuration</td>
    <td style="padding:15px">{base_path}/{version}/v1/appstore/apps/{pathv1}/install/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uninstallAppUsingDELETE(appId, request, callback)</td>
    <td style="padding:15px">Delete app</td>
    <td style="padding:15px">{base_path}/{version}/v1/appstore/apps/{pathv1}/uninstall?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradeAppUsingPOST(appId, request, callback)</td>
    <td style="padding:15px">Upgrade app</td>
    <td style="padding:15px">{base_path}/{version}/v1/appstore/apps/{pathv1}/upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">developersSearchAppsUsingGET(collectorId, includeLiveVersion, keywords, callback)</td>
    <td style="padding:15px">Developer apps</td>
    <td style="padding:15px">{base_path}/{version}/v1/appstore/developers/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDeveloperInstalledAppUsingPOST(appId, request, callback)</td>
    <td style="padding:15px">Edit Developer installed app configuration</td>
    <td style="padding:15px">{base_path}/{version}/v1/appstore/developers/apps/{pathv1}/install/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeveloperAppUsingGET(appId, collectorId, version, callback)</td>
    <td style="padding:15px">Get Developer App</td>
    <td style="padding:15px">{base_path}/{version}/v1/appstore/developers/apps/{pathv1}/versions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeveloperAppIconUsingGET(appId, version, callback)</td>
    <td style="padding:15px">Get Developer App Icon</td>
    <td style="padding:15px">{base_path}/{version}/v1/appstore/developers/apps/{pathv1}/versions/{pathv2}/icon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">installDeveloperAppUsingPOST(appId, request, version, callback)</td>
    <td style="padding:15px">Install Developer app</td>
    <td style="padding:15px">{base_path}/{version}/v1/appstore/developers/apps/{pathv1}/versions/{pathv2}/install?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uninstallDeveloperAppUsingDELETE(appId, request, version, callback)</td>
    <td style="padding:15px">Delete Developer app</td>
    <td style="padding:15px">{base_path}/{version}/v1/appstore/developers/apps/{pathv1}/versions/{pathv2}/uninstall?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradeDeveloperAppUsingPOST(appId, request, version, callback)</td>
    <td style="padding:15px">Upgrade developer app</td>
    <td style="padding:15px">{base_path}/{version}/v1/appstore/developers/apps/{pathv1}/versions/{pathv2}/upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">developerInfoUsingGET(callback)</td>
    <td style="padding:15px">Developer info</td>
    <td style="padding:15px">{base_path}/{version}/v1/appstore/developers/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findAllRecommendedAppsUsingGET(collectorId, page, size, sort, type, callback)</td>
    <td style="padding:15px">Find Recommended Apps</td>
    <td style="padding:15px">{base_path}/{version}/v1/appstore/recommendations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllGatewaysUsingGET(checkStatus, page, size, sort, callback)</td>
    <td style="padding:15px">Return Pageable List of Gateways</td>
    <td style="padding:15px">{base_path}/{version}/v1/iot_gateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllOnlineGatewaysUsingGET(checkStatus, page, size, sort, callback)</td>
    <td style="padding:15px">Return Pageable List of Gateways</td>
    <td style="padding:15px">{base_path}/{version}/v1/iot_gateways/online?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGatewayUsingGET(checkStatus, id, callback)</td>
    <td style="padding:15px">Return a Gateway by cluster Id</td>
    <td style="padding:15px">{base_path}/{version}/v1/iot_gateways/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAssociationUsingGET(page, size, sort, callback)</td>
    <td style="padding:15px">Return Pageable List of Gateways</td>
    <td style="padding:15px">{base_path}/{version}/v1/iot_gateways/association?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAssociationUsingPOST(request, callback)</td>
    <td style="padding:15px">Add Association</td>
    <td style="padding:15px">{base_path}/{version}/v1/iot_gateways/association?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAssociationUsingDELETE(request, callback)</td>
    <td style="padding:15px">Delete Association</td>
    <td style="padding:15px">{base_path}/{version}/v1/iot_gateways/association?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssociationForCollectorUsingGET(collectorId, callback)</td>
    <td style="padding:15px">Return Pageable List of Gateways</td>
    <td style="padding:15px">{base_path}/{version}/v1/iot_gateways/association/collector/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssociationTokenForCollectorUsingGET(collectorId, callback)</td>
    <td style="padding:15px">Returns security token associated with a collector</td>
    <td style="padding:15px">{base_path}/{version}/v1/iot_gateways/association/token/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findTransportProfilesUsingGET(collectorId, page, size, sort, callback)</td>
    <td style="padding:15px">Find transport profiles</td>
    <td style="padding:15px">{base_path}/{version}/v1/transport_profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUsingPOST(request, callback)</td>
    <td style="padding:15px">Create a transport profile</td>
    <td style="padding:15px">{base_path}/{version}/v1/transport_profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countTransportProfilesUsingGET(collectorIds, callback)</td>
    <td style="padding:15px">Count transport profiles</td>
    <td style="padding:15px">{base_path}/{version}/v1/transport_profiles/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findDeviceClassesUsingGET(callback)</td>
    <td style="padding:15px">Get a list of all device classes</td>
    <td style="padding:15px">{base_path}/{version}/v1/transport_profiles/device_classes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsingGET(collectorId, transportProfileId, callback)</td>
    <td style="padding:15px">Get a transport profile by id</td>
    <td style="padding:15px">{base_path}/{version}/v1/transport_profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveUsingPUT(request, transportProfileId, callback)</td>
    <td style="padding:15px">Update a transport profile by id</td>
    <td style="padding:15px">{base_path}/{version}/v1/transport_profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTransportUsingDELETE(request, transportProfileId, callback)</td>
    <td style="padding:15px">Delete a transport profile by id</td>
    <td style="padding:15px">{base_path}/{version}/v1/transport_profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetNetworksV2(group, swarmId, label, site, calculateClientCount, sort, callback)</td>
    <td style="padding:15px">List all Networks</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v2/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetNetworkV2(networkName, group, swarmId, label, site, callback)</td>
    <td style="padding:15px">Get Network details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v2/networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetNetworksBandwidthUsageV2(network, group, swarmId, label, fromTimestamp, toTimestamp, site, callback)</td>
    <td style="padding:15px">WLAN Network Bandwidth usage</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v2/networks/bandwidth_usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetSwarmsBandwidthUsageTopn(group, count, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">Top N Swarms By Bandwidth Usage</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/swarms/bandwidth_usage/topn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetSwarmsClientsCountTopn(group, count, fromTimestamp, toTimestamp, sort, callback)</td>
    <td style="padding:15px">Top N Swarms By Clients Count</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/swarms/clients_count/topn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetSwarms(group, status, publicIpAddress, fields, calculateTotal, offset, limit, sort, swarmName, callback)</td>
    <td style="padding:15px">List Swarms</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/swarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetSwarm(swarmId, callback)</td>
    <td style="padding:15px">Swarm Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/swarms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetWirelessClients(group, swarmId, label, site, network, serial, osType, clusterId, band, fields, calculateTotal, offset, limit, sort, lastClientMac, callback)</td>
    <td style="padding:15px">List Wireless Clients</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/clients/wireless?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetWiredClients(group, swarmId, label, site, serial, clusterId, stackId, fields, calculateTotal, offset, limit, sort, lastClientMac, callback)</td>
    <td style="padding:15px">List Wired Clients</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/clients/wired?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetV2UnifiedClients(group, swarmId, label, site, network, serial, clusterId, band, stackId, osType, fields, calculateTotal, offset, limit, sort, lastClientMac, timerange = '3H', clientType = 'WIRELESS', clientStatus = 'CONNECTED', showUsage, showManufacturer, showSignalDb, callback)</td>
    <td style="padding:15px">List Unified Clients (Wired/Wireless). Option to choose Connected/Failed Clients</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v2/clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetV2UnifiedClientDetail(macaddr, callback)</td>
    <td style="padding:15px">Client Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v2/clients/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetWirelessClient(macaddr, callback)</td>
    <td style="padding:15px">Wireless Client Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/clients/wireless/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetWirelessClientMobility(macaddr, calculateTotal, offset, limit, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">Wireless Client Mobility Trail</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/clients/wireless/{pathv1}/mobility_trail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetWiredClient(macaddr, callback)</td>
    <td style="padding:15px">Wired Client Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/clients/wired/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetClientsBandwidthUsage(group, swarmId, label, clusterId, stackId, serial, macaddr, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">Client Bandwidth Usage</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/clients/bandwidth_usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetClientsBandwidthUsageTopn(group, swarmId, label, network, clusterId, stackId, count, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">Top N Clients</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/clients/bandwidth_usage/topn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetClientsCount(group, swarmId, label, network, clusterId, stackId, deviceType, serial, band, radioNumber, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">Total Clients Count</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/clients/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetVpnInfo(swarmId, callback)</td>
    <td style="padding:15px">Vpn Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/vpn/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetVpnUsageV3(body, callback)</td>
    <td style="padding:15px">Swarm VPN stats</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v3/vpn/usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetMcPortsBandwidthUsage(serial, fromTimestamp, toTimestamp, port, callback)</td>
    <td style="padding:15px">Mobility Controllers Ports Bandwidth Usage</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/mobility_controllers/{pathv1}/ports/bandwidth_usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetMcPorts(serial, callback)</td>
    <td style="padding:15px">Mobility Controllers Ports Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/mobility_controllers/{pathv1}/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetVlanInfo(serial, callback)</td>
    <td style="padding:15px">Mobility Controllers VLAN details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/mobility_controllers/{pathv1}/vlan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetMcsV2(group, label, site, status, macaddr, model, fields, calculateTotal, offset, limit, sort, callback)</td>
    <td style="padding:15px">List Mobility Controllers</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v2/mobility_controllers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetMcV2(serial, statsMetric, callback)</td>
    <td style="padding:15px">Mobility Controller Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v2/mobility_controllers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerDeleteMcV2(serial, callback)</td>
    <td style="padding:15px">Delete Mobility Controller</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v2/mobility_controllers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetGateways(group, label, site, status, macaddr, model, fields, calculateTotal, offset, limit, sort, callback)</td>
    <td style="padding:15px">Gateway List</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/gateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetGateway(serial, statsMetric, callback)</td>
    <td style="padding:15px">Gateway Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/gateways/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerDeleteGateway(serial, callback)</td>
    <td style="padding:15px">Delete Gateway</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/gateways/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetGatewayUplinksDetail(serial, timerange = '3H', callback)</td>
    <td style="padding:15px">Gateway Uplink Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/gateways/{pathv1}/uplinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetGatewayUplinksBandwidthUsage(serial, uplinkId, interval, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">Gateway Uplink Bandwidth Usage</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/gateways/{pathv1}/uplinks/bandwidth_usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetGatewayUplinksTunnelStats(serial, uplinkId, mapName, interval, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">Gateway Uplink tunnel stats</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/gateways/{pathv1}/tunnels/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetGatewayUplinksWanCompressionUsage(serial, uplinkId, interval, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">Gateway Uplink WAN compression stats</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/gateways/{pathv1}/uplinks/wan_compression_stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetGatewayUplinksDistribution(serial, callback)</td>
    <td style="padding:15px">Gateway Uplink distribution</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/gateways/{pathv1}/uplinks/distribution?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetGatewayPortsBandwidthUsage(serial, fromTimestamp, toTimestamp, port, callback)</td>
    <td style="padding:15px">Gateway Ports Bandwidth Usage</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/gateways/{pathv1}/ports/bandwidth_usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetGatewayPorts(serial, callback)</td>
    <td style="padding:15px">Gateway Ports Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/gateways/{pathv1}/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetGatewayPortErrors(serial, port, interval = '5minutes', fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">Gateway Port Errors</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/gateways/{pathv1}/ports/errors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetGatewayTunnels(serial, timerange = '3H', offset, limit, callback)</td>
    <td style="padding:15px">Gateway Tunnels Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/gateways/{pathv1}/tunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetGatewayDhcpClients(serial, reservation, offset, limit, callback)</td>
    <td style="padding:15px">Gateway DHCP Clients information</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/gateways/{pathv1}/dhcp_clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetGatewayDhcpPools(serial, callback)</td>
    <td style="padding:15px">Gateway DHCP Pools details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/gateways/{pathv1}/dhcp_pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetGatewayVlanInfo(serial, callback)</td>
    <td style="padding:15px">Gateway VLAN details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/gateways/{pathv1}/vlan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">labelsExternalControllerGetLabels(calculateTotal, categoryId, offset, limit, sort, callback)</td>
    <td style="padding:15px">List Labels</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">labelsExternalControllerCreateLabel(body, callback)</td>
    <td style="padding:15px">Create Label</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">labelsExternalControllerGetDefaultLabels(calculateTotal, offset, limit, sort, callback)</td>
    <td style="padding:15px">List Default Labels</td>
    <td style="padding:15px">{base_path}/{version}/central/v2/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">labelsExternalControllerGetLabel(labelId, callback)</td>
    <td style="padding:15px">Label details</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/labels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">labelsExternalControllerUpdateLabel(labelId, body, callback)</td>
    <td style="padding:15px">Update Label</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/labels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">labelsExternalControllerDeleteLabel(labelId, callback)</td>
    <td style="padding:15px">Delete Label</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/labels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">labelsExternalControllerAssignLabel(body, callback)</td>
    <td style="padding:15px">Associate Label to device</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/labels/associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">labelsExternalControllerUnassignLabel(body, callback)</td>
    <td style="padding:15px">Unassociate Label from device</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/labels/associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">labelsExternalControllerAssignLabelToDevices(body, callback)</td>
    <td style="padding:15px">Associate Label to a list of devices</td>
    <td style="padding:15px">{base_path}/{version}/central/v2/labels/associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">labelsExternalControllerUnassignLabelFromDevices(body, callback)</td>
    <td style="padding:15px">Unassociate a label from a list of devices</td>
    <td style="padding:15px">{base_path}/{version}/central/v2/labels/associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">labelsExternalControllerGetLabelCategories(calculateTotal, limit, offset, callback)</td>
    <td style="padding:15px">List Label Categories</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/labels/categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sitesExternalControllerGetSites(calculateTotal, offset, limit, sort, callback)</td>
    <td style="padding:15px">List Sites</td>
    <td style="padding:15px">{base_path}/{version}/central/v2/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sitesExternalControllerCreateSite(body, callback)</td>
    <td style="padding:15px">Create Site</td>
    <td style="padding:15px">{base_path}/{version}/central/v2/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sitesExternalControllerGetSite(siteId, callback)</td>
    <td style="padding:15px">Site details</td>
    <td style="padding:15px">{base_path}/{version}/central/v2/sites/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sitesExternalControllerUpdateSite(siteId, body, callback)</td>
    <td style="padding:15px">Update Site</td>
    <td style="padding:15px">{base_path}/{version}/central/v2/sites/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sitesExternalControllerDeleteSite(siteId, callback)</td>
    <td style="padding:15px">Delete Site</td>
    <td style="padding:15px">{base_path}/{version}/central/v2/sites/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sitesExternalControllerAssignSite(body, callback)</td>
    <td style="padding:15px">Associate Site to device</td>
    <td style="padding:15px">{base_path}/{version}/central/v2/sites/associate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sitesExternalControllerUnassignSite(body, callback)</td>
    <td style="padding:15px">Unassociate Site from device</td>
    <td style="padding:15px">{base_path}/{version}/central/v2/sites/associate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sitesExternalControllerAssignSiteToDevices(body, callback)</td>
    <td style="padding:15px">Associate Site to a list of devices</td>
    <td style="padding:15px">{base_path}/{version}/central/v2/sites/associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sitesExternalControllerUnassignSiteFromDevices(body, callback)</td>
    <td style="padding:15px">Unassociate a site from a list of devices</td>
    <td style="padding:15px">{base_path}/{version}/central/v2/sites/associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetSwitches(group, label, stackId, status, fields, calculateTotal, showResourceDetails, calculateClientCount, publicIpAddress, site, offset, limit, sort, callback)</td>
    <td style="padding:15px">List Switches</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetSwitchVlan(serial, name, id, taggedPort, untaggedPort, isJumboEnabled, isVoiceEnabled, isIgmpEnabled, type, primaryVlanId, status, offset, limit, sort, calculateTotal, callback)</td>
    <td style="padding:15px">Get vlan info of the switch</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/switches/{pathv1}/vlan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetCxSwitchVlan(serial, name, id, taggedPort, untaggedPort, isJumboEnabled, isVoiceEnabled, isIgmpEnabled, type, primaryVlanId, status, offset, limit, sort, calculateTotal, callback)</td>
    <td style="padding:15px">Get vlan info for CX switch</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/cx_switches/{pathv1}/vlan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetSwitchStackVlan(stackId, name, id, taggedPort, untaggedPort, isJumboEnabled, isVoiceEnabled, isIgmpEnabled, type, primaryVlanId, status, offset, limit, sort, calculateTotal, callback)</td>
    <td style="padding:15px">Get vlan info of the switch stack</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/switch_stacks/{pathv1}/vlan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetCxSwitchStackVlan(stackId, name, id, taggedPort, untaggedPort, isJumboEnabled, isVoiceEnabled, isIgmpEnabled, type, primaryVlanId, status, offset, limit, sort, calculateTotal, callback)</td>
    <td style="padding:15px">Get vlan info of the CX switch stack</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/cx_switch_stacks/{pathv1}/vlan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetSwitchPoeDetail(serial, port, callback)</td>
    <td style="padding:15px">Get switch port poe info</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/switches/{pathv1}/poe_detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetCxSwitchPoeDetail(serial, port, callback)</td>
    <td style="padding:15px">Get switch port poe info for CX switch</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/cx_switches/{pathv1}/poe_detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetSwitchPoeDetailsForAllPorts(serial, port, callback)</td>
    <td style="padding:15px">Get switch poe info</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/switches/{pathv1}/poe_details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetCxSwitchPoeDetailsForAllPorts(serial, port, callback)</td>
    <td style="padding:15px">Get switch poe info for CX switch</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/cx_switches/{pathv1}/poe_details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetSwitchVsxDetail(serial, callback)</td>
    <td style="padding:15px">Get switch vsx info for CX switch</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/cx_switches/{pathv1}/vsx?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetBandwidthUsage(group, label, serial, stackId, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">Switch Bandwidth Usage</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/switches/bandwidth_usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetBandwidthUsageTopn(group, label, stackId, count, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">Top N Switches</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/switches/bandwidth_usage/topn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetSwitch(serial, callback)</td>
    <td style="padding:15px">Switch Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/switches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerDeleteSwitch(serial, callback)</td>
    <td style="padding:15px">Delete Switch</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/switches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetSwitchPorts(serial, slot, callback)</td>
    <td style="padding:15px">Switch Ports Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/switches/{pathv1}/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetCxSwitchPorts(serial, slot, callback)</td>
    <td style="padding:15px">Get ports details for CX switch</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/cx_switches/{pathv1}/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetChassisInfo(serial, callback)</td>
    <td style="padding:15px">Switch Chassis Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/switches/{pathv1}/chassis_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetSwitchPortsBandwidthUsage(serial, fromTimestamp, toTimestamp, port, showUplink, callback)</td>
    <td style="padding:15px">Switch Ports Bandwidth Usage</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/switches/{pathv1}/ports/bandwidth_usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetCxSwitchPortsBandwidthUsage(serial, fromTimestamp, toTimestamp, port, showUplink, callback)</td>
    <td style="padding:15px">Ports Bandwidth Usage for CX Switch</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/cx_switches/{pathv1}/ports/bandwidth_usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetPortsErrors(serial, fromTimestamp, toTimestamp, port, error, callback)</td>
    <td style="padding:15px">Switch Ports Errors</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/switches/{pathv1}/ports/errors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetCxPortsErrors(serial, fromTimestamp, toTimestamp, port, error, callback)</td>
    <td style="padding:15px">CX Switch Ports Errors</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/cx_switches/{pathv1}/ports/errors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetStackPorts(stackId, callback)</td>
    <td style="padding:15px">Switch Stack Port Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/switch_stacks/{pathv1}/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetCxStackPorts(stackId, callback)</td>
    <td style="padding:15px">CX Switch Stack Port Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/cx_switch_stacks/{pathv1}/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetSwitchStacks(hostname, group, offset, limit, callback)</td>
    <td style="padding:15px">List Switch Stacks</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/switch_stacks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetSwitchStack(stackId, callback)</td>
    <td style="padding:15px">Switch Stack Details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/switch_stacks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerDeleteSwitchStack(stackId, callback)</td>
    <td style="padding:15px">Delete Switch Stack</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v1/switch_stacks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetEventsV2(group, swarmId, label, fromTimestamp, toTimestamp, offset, limit, macaddr, bssid, deviceMac, hostname, deviceType = 'ACCESS POINT', sort = '-timestamp', site, serial, level, eventDescription, eventType, fields, calculateTotal, callback)</td>
    <td style="padding:15px">List Events</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/v2/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsMspApiGetCustomers(offset, limit, customerName, callback)</td>
    <td style="padding:15px">Get list of customers under the MSP account based on limit and offset</td>
    <td style="padding:15px">{base_path}/{version}/msp_api/v1/customers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsMspApiCreateCustomer(customer, callback)</td>
    <td style="padding:15px">Create a new customer</td>
    <td style="padding:15px">{base_path}/{version}/msp_api/v1/customers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsMspApiGetCustomer(customerId, callback)</td>
    <td style="padding:15px">Get details of customer</td>
    <td style="padding:15px">{base_path}/{version}/msp_api/v1/customers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsMspApiEditCustomer(customerId, customer, callback)</td>
    <td style="padding:15px">Update a customer</td>
    <td style="padding:15px">{base_path}/{version}/msp_api/v1/customers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsMspApiDeleteCustomer(customerId, callback)</td>
    <td style="padding:15px">Delete a customer</td>
    <td style="padding:15px">{base_path}/{version}/msp_api/v1/customers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsMspApiGetMspUsers(offset, limit, callback)</td>
    <td style="padding:15px">Get list of users under the MSP account based on limit and offset</td>
    <td style="padding:15px">{base_path}/{version}/msp_api/v1/customers/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsMspApiGetCustomerUsers(customerId, offset, limit, callback)</td>
    <td style="padding:15px">Get list of users under the Customer account based on limit and offset</td>
    <td style="padding:15px">{base_path}/{version}/msp_api/v1/customers/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiUserGetUserAccounts(limit, offset, orderBy = '+username', appName, type = 'system', status = 'inprogress', callback)</td>
    <td style="padding:15px">List user accounts</td>
    <td style="padding:15px">{base_path}/{version}/platform/rbac/v1/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiUserCreateUserAccount(params, callback)</td>
    <td style="padding:15px">Create a user account</td>
    <td style="padding:15px">{base_path}/{version}/platform/rbac/v1/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiUserUpdateUserAccount(params, userId, callback)</td>
    <td style="padding:15px">update user account details specified by user id.Providing info on account setting app is mandatory</td>
    <td style="padding:15px">{base_path}/{version}/platform/rbac/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiUserGetUserAccountDetails(userId, systemUser, callback)</td>
    <td style="padding:15px">Get user account details specified by user id</td>
    <td style="padding:15px">{base_path}/{version}/platform/rbac/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiUserDeleteUserAccount(userId, systemUser, callback)</td>
    <td style="padding:15px">delete user account details specified by user id</td>
    <td style="padding:15px">{base_path}/{version}/platform/rbac/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiUserChangeUserPassword(params, userId, callback)</td>
    <td style="padding:15px">Change user password</td>
    <td style="padding:15px">{base_path}/{version}/platform/rbac/v1/users/{pathv1}/password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiUserResetUserPassword(params, userId, callback)</td>
    <td style="padding:15px">Reset user password</td>
    <td style="padding:15px">{base_path}/{version}/platform/rbac/v1/users/{pathv1}/password/reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsMspApiGetResource(callback)</td>
    <td style="padding:15px">Get the resource under the MSP</td>
    <td style="padding:15px">{base_path}/{version}/msp_api/v1/resource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsMspApiEditResource(resource, callback)</td>
    <td style="padding:15px">Edit an existing resource for the MSP</td>
    <td style="padding:15px">{base_path}/{version}/msp_api/v1/resource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiGetLabelsApi(offset, limit, name, column, order = 'asc', sitePropertiesUsedWithThresholds = 'gt  (Greater than)', callback)</td>
    <td style="padding:15px">Get data for all labels</td>
    <td style="padding:15px">{base_path}/{version}/branchhealth/v1/label?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsExternalApiGetSitesApi(offset, limit, name, column, order = 'asc', sitePropertiesUsedWithThresholds = 'gt  (Greater than)', callback)</td>
    <td style="padding:15px">Get data for all sites</td>
    <td style="padding:15px">{base_path}/{version}/branchhealth/v1/site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiNotificationsExternalApiGetTypesApi(calculateTotal, offset, limit, callback)</td>
    <td style="padding:15px">List Types</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/notifications/types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiNotificationsExternalApiGetSettingsApi(search, limit, offset, sort = '-created_ts', callback)</td>
    <td style="padding:15px">List Settings</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/notifications/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiNotificationsExternalApiAddSettingApi(body, callback)</td>
    <td style="padding:15px">Add settings</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/notifications/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiNotificationsExternalApiDeleteSettingApi(settingsId, callback)</td>
    <td style="padding:15px">Delete Settings</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/notifications/settings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiNotificationsExternalApiUpdateSettingApi(settingsId, body, callback)</td>
    <td style="padding:15px">Update settings details</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/notifications/settings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiNotificationsExternalApiGetCustomerSettingsApi(callback)</td>
    <td style="padding:15px">Get Customer account level Settings</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/notifications/customer_settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiNotificationsExternalApiUpdateCustomerSettingsApi(requestData, callback)</td>
    <td style="padding:15px">Update customer settings</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/notifications/customer_settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiNotificationsExternalApiGetCountBySeverityApi(customerId, group, label, serial, site, fromTimestamp, toTimestamp, ack, callback)</td>
    <td style="padding:15px">Get notifications count by severity</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/notifications/count_by_severity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiNotificationsExternalApiGetNotificationsApi(customerId, group, label, serial, site, fromTimestamp, toTimestamp, severity, type, search, calculateTotal, ack, fields, offset, limit, callback)</td>
    <td style="padding:15px">List Notifications</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/notifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiNotificationsExternalApiAcknowledgeNotifications(body, callback)</td>
    <td style="padding:15px">Acknowledge Notifications by ID List / All</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/notifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiNotificationsExternalApiAcknowledgeNotification(notificationId, body, callback)</td>
    <td style="padding:15px">Acknowledge Notification</td>
    <td style="padding:15px">{base_path}/{version}/central/v1/notifications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiEnableWildcardFlow(body, callback)</td>
    <td style="padding:15px">Enable/Disable the Syslog App.</td>
    <td style="padding:15px">{base_path}/{version}/ofcapi/v1/syslog/flow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiEnableWildcardFlowList(enable, serialIdMetadata, callback)</td>
    <td style="padding:15px">Enable Syslog App on a list of given device SerialIDs.</td>
    <td style="padding:15px">{base_path}/{version}/ofcapi/v1/syslog/flows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCheckStatusList(serialIds, callback)</td>
    <td style="padding:15px">Check Status of Syslog App for given SerialIDs.</td>
    <td style="padding:15px">{base_path}/{version}/ofcapi/v1/syslog/flows/status/device_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiCheckStatus(serialId, callback)</td>
    <td style="padding:15px">Check Status of Enabled Flow SerialID</td>
    <td style="padding:15px">{base_path}/{version}/ofcapi/v1/syslog/flow/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceNbApiApiDeviceInventoryGetDevice(serial, callback)</td>
    <td style="padding:15px">Get device from device inventory</td>
    <td style="padding:15px">{base_path}/{version}/platform/device_inventory/v1/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceNbApiApiDeviceInventoryGetMspCustomerDevices(customerId, offset, limit, deviceType, callback)</td>
    <td style="padding:15px">A filterable paginated response of a list of devices and licenses under the customer account based</td>
    <td style="padding:15px">{base_path}/{version}/platform/device_inventory/v1/msp/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceNbApiApiDeviceInventoryAssignDeviceToCustomer(customerId, devices, callback)</td>
    <td style="padding:15px">assign the device to the end-customer</td>
    <td style="padding:15px">{base_path}/{version}/platform/device_inventory/v1/msp/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceNbApiApiDeviceInventoryGetMspDevices(offset, limit, deviceType, customerName, deviceAllocationStatus, callback)</td>
    <td style="padding:15px">A filterable paginated response of a list of devices and licenses under the MSP account based on th</td>
    <td style="padding:15px">{base_path}/{version}/platform/device_inventory/v1/msp/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceLicenseWebViewsAdminLicenseGetUserSubscriptions(limit, offset, licenseType, callback)</td>
    <td style="padding:15px">Get user subscription keys</td>
    <td style="padding:15px">{base_path}/{version}/platform/licensing/v1/subscriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceLicenseWebViewsAdminLicenseGetSubscriptionStats(licenseType, service, appOnlyStats, callback)</td>
    <td style="padding:15px">Get subscription stats</td>
    <td style="padding:15px">{base_path}/{version}/platform/licensing/v1/subscriptions/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceLicenseWebViewsAdminLicenseApigwAssignLicenses(params, callback)</td>
    <td style="padding:15px">Assign subscription to a device</td>
    <td style="padding:15px">{base_path}/{version}/platform/licensing/v1/subscriptions/assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceLicenseWebViewsAdminLicenseApigwUnassignLicenses(params, callback)</td>
    <td style="padding:15px">Unassign subscription to a device</td>
    <td style="padding:15px">{base_path}/{version}/platform/licensing/v1/subscriptions/unassign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceLicenseWebViewsAdminLicenseApigwGetCustomerEnabledServices(callback)</td>
    <td style="padding:15px">Get enabled services for customer</td>
    <td style="padding:15px">{base_path}/{version}/platform/licensing/v1/services/enabled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceLicenseWebViewsAdminLicenseGetServicesConfig(serviceCategory, deviceType, callback)</td>
    <td style="padding:15px">Get services licensing config</td>
    <td style="padding:15px">{base_path}/{version}/platform/licensing/v1/services/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceLicenseWebViewsAdminLicenseAssignSubscriptionAllDevices(params, callback)</td>
    <td style="padding:15px">Standalone customer API:- Assign licenses to all devices.</td>
    <td style="padding:15px">{base_path}/{version}/platform/licensing/v1/subscriptions/devices/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceLicenseWebViewsAdminLicenseUnassignSubscriptionAllDevices(params, callback)</td>
    <td style="padding:15px">Standalone customer API:- Un-assign licenses to all devices for given services.</td>
    <td style="padding:15px">{base_path}/{version}/platform/licensing/v1/subscriptions/devices/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceLicenseWebViewsAdminLicenseMspAssignSubscriptionAllDevices(params, callback)</td>
    <td style="padding:15px">MSP API:- Assign licenses to all the devices owned by tenant customers.</td>
    <td style="padding:15px">{base_path}/{version}/platform/licensing/v1/msp/subscriptions/devices/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceLicenseWebViewsAdminLicenseMspUnassignSubscriptionAllDevices(params, callback)</td>
    <td style="padding:15px">MSP API:- Remove service licenses to all the devices owned by tenants and MSP.</td>
    <td style="padding:15px">{base_path}/{version}/platform/licensing/v1/msp/subscriptions/devices/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceLicenseWebViewsAdminLicenseApigwLicenseAvailable(service, callback)</td>
    <td style="padding:15px">Get services and corresponding license token availability status.</td>
    <td style="padding:15px">{base_path}/{version}/platform/licensing/v1/autolicensing/services/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceLicenseWebViewsAdminLicenseApigwGetAutolicenseSettings(callback)</td>
    <td style="padding:15px">Get the services which are auto enabled.</td>
    <td style="padding:15px">{base_path}/{version}/platform/licensing/v1/customer/settings/autolicense?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceLicenseWebViewsAdminLicenseApigwEnableAutoLicensingSettings(services, callback)</td>
    <td style="padding:15px">Standalone Customer API:- Assign licenses to all devices and enable auto licensing for given servic</td>
    <td style="padding:15px">{base_path}/{version}/platform/licensing/v1/customer/settings/autolicense?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceLicenseWebViewsAdminLicenseApigwDisableAutoLicensingSettings(params, callback)</td>
    <td style="padding:15px">Standalone Customer API:- Disable auto licensing for given services.</td>
    <td style="padding:15px">{base_path}/{version}/platform/licensing/v1/customer/settings/autolicense?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceLicenseWebViewsAdminLicenseApigwMspGetAutolicenseSettings(customerId, callback)</td>
    <td style="padding:15px">Get auto enabled services for msp or tenant customer.</td>
    <td style="padding:15px">{base_path}/{version}/platform/licensing/v1/msp/customer/settings/autolicense?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceLicenseWebViewsAdminLicenseApigwMspEnableAutoLicensingSettings(services, callback)</td>
    <td style="padding:15px">MSP API:- Enable auto license settings and assign services to all devices owned by tenant customers.</td>
    <td style="padding:15px">{base_path}/{version}/platform/licensing/v1/msp/customer/settings/autolicense?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acpServiceLicenseWebViewsAdminLicenseApigwMspDisableAutoLicensingSettings(params, callback)</td>
    <td style="padding:15px">MSP API:- Disable auto license settings at msp and its tenant level for given services.</td>
    <td style="padding:15px">{base_path}/{version}/platform/licensing/v1/msp/customer/settings/autolicense?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsPresenceSetPresenceV3Thresholds(body, selectAll, callback)</td>
    <td style="padding:15px">It configures RSSI threshold, dwelltime threshold for visitor & RSSI threshold for passerby.</td>
    <td style="padding:15px">{base_path}/{version}/presence/v3/config/thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsPresenceGetPresenceV3Thresholds(siteId, callback)</td>
    <td style="padding:15px">It retrieves RSSI threshold for passerby conversion, RSSI threshold for visitor conversion & dwellt</td>
    <td style="padding:15px">{base_path}/{version}/presence/v3/config/thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsPresenceGetPaConfigData(offset, limit, sort = 'asc', search, siteId, callback)</td>
    <td style="padding:15px">It retrieves visitor RSSI threshold, passerby RSSI threshold, dwell time threshold, access points a</td>
    <td style="padding:15px">{base_path}/{version}/presence/v3/sites/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsPresenceGetVisitorsStatusInfo(startTime, endTime, tagId, callback)</td>
    <td style="padding:15px">Get details of connected and non connected visitors.</td>
    <td style="padding:15px">{base_path}/{version}/presence/v3/visitor_status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsPresenceGetLoyaltyVisitFrequency(tagId, startTime, endTime, callback)</td>
    <td style="padding:15px">Get loyalty visitors frequency.</td>
    <td style="padding:15px">{base_path}/{version}/presence/v3/visit_frequency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsPresenceGetV3LoyaltyTrends(tagId, startTime, endTime, sampleFrequency = 'hourly', callback)</td>
    <td style="padding:15px">Get presence analytics trends.</td>
    <td style="padding:15px">{base_path}/{version}/presence/v3/analytics/trends/loyal_visitors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsPresenceGetDashboardV3PercentileDatapoints(tagId, category, startTime, endTime, sampleFrequency = 'hourly', callback)</td>
    <td style="padding:15px">Get presence analytics trends.</td>
    <td style="padding:15px">{base_path}/{version}/presence/v3/analytics/trends/passerby_visitors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsPresenceEnableOrDisablePaLicense(body, callback)</td>
    <td style="padding:15px">Enable or disable PA license.</td>
    <td style="padding:15px">{base_path}/{version}/presence/v3/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsPresenceGetPaLicenseStatus(callback)</td>
    <td style="padding:15px">Customer level device license status.</td>
    <td style="padding:15px">{base_path}/{version}/presence/v3/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsPresenceGetDeviceLicenseStatusPerSite(tagId, callback)</td>
    <td style="padding:15px">List of devices per site with their pa license status.</td>
    <td style="padding:15px">{base_path}/{version}/presence/v3/sites/devicelicense?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsPresenceGetPresenceSiteWiseData(startTime, endTime, offset, limit, search, sort = 'asc', siteId, callback)</td>
    <td style="padding:15px">Get presence aggregate values for list of sites.</td>
    <td style="padding:15px">{base_path}/{version}/presence/v3/insights/sites/aggregates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ucDshbViewsGetUcInsights(label, startTime, endTime, offset, limit, callback)</td>
    <td style="padding:15px">Fetch insights for a day. start time 00:00:00 and end time 23:59:59</td>
    <td style="padding:15px">{base_path}/{version}/v1/insights?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRapidsV1RogueAps(group, label, site, start, end, offset, limit, swarmId, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">List Rogue APs</td>
    <td style="padding:15px">{base_path}/{version}/rapids/v1/rogue_aps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRapidsV1InterferingAps(group, label, site, start, end, offset, limit, swarmId, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">List Interfering APs</td>
    <td style="padding:15px">{base_path}/{version}/rapids/v1/interfering_aps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRapidsV1SuspectAps(group, label, site, start, end, offset, limit, swarmId, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">List suspect APs</td>
    <td style="padding:15px">{base_path}/{version}/rapids/v1/suspect_aps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRapidsV1NeighborAps(group, label, site, start, end, offset, limit, swarmId, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">List neighbor APs</td>
    <td style="padding:15px">{base_path}/{version}/rapids/v1/neighbor_aps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRapidsV1ManuallyContainedAps(group, label, site, start, end, offset, limit, swarmId, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">List manually contained APs</td>
    <td style="padding:15px">{base_path}/{version}/rapids/v1/manually_contained_aps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRapidsV1SsidAllow(callback)</td>
    <td style="padding:15px">List Allowed SSIDs</td>
    <td style="padding:15px">{base_path}/{version}/rapids/v1/ssid_allow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRapidsV1SsidAllow(ssidsManuallyAllowed, callback)</td>
    <td style="padding:15px">Add Allowed SSIDs</td>
    <td style="padding:15px">{base_path}/{version}/rapids/v1/ssid_allow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRapidsV1SsidAllow(ssidsManuallyAllowed, callback)</td>
    <td style="padding:15px">Delete Allowed SSIDs</td>
    <td style="padding:15px">{base_path}/{version}/rapids/v1/ssid_allow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRapidsV1SsidBlock(callback)</td>
    <td style="padding:15px">List Blocked SSIDs</td>
    <td style="padding:15px">{base_path}/{version}/rapids/v1/ssid_block?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRapidsV1SsidBlock(ssidsManuallyBlocked, callback)</td>
    <td style="padding:15px">Add Blocked SSIDs</td>
    <td style="padding:15px">{base_path}/{version}/rapids/v1/ssid_block?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRapidsV1SsidBlock(ssidsManuallyBlocked, callback)</td>
    <td style="padding:15px">Delete Blocked SSIDs</td>
    <td style="padding:15px">{base_path}/{version}/rapids/v1/ssid_block?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetInfrastructureAttacks(group, label, site, start, end, calculateTotal, offset, limit, sort = '-ts', swarmId, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">List Infrastructure Attacks</td>
    <td style="padding:15px">{base_path}/{version}/rapids/v1/wids/infrastructure_attacks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetClientAttacks(group, label, site, start, end, calculateTotal, offset, limit, sort = '-ts', swarmId, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">List Client Attacks</td>
    <td style="padding:15px">{base_path}/{version}/rapids/v1/wids/client_attacks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalControllerGetWidsEvents(group, label, site, start, end, offset, limit, sort = '-ts', swarmId, fromTimestamp, toTimestamp, callback)</td>
    <td style="padding:15px">WIDS Events</td>
    <td style="padding:15px">{base_path}/{version}/rapids/v1/wids/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">xXXXXX(clientId, clientSecret, grantType, refreshToken, callback)</td>
    <td style="padding:15px">Refresh API token</td>
    <td style="padding:15px">{base_path}/{version}/oauth2/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalApiScheduledReports(cid, startTime, endTime, callback)</td>
    <td style="padding:15px">Scheduled Reports</td>
    <td style="padding:15px">{base_path}/{version}/reports/api/v1/{pathv1}/scheduled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiExternalApiGeneratedReports(cid, startTime, endTime, callback)</td>
    <td style="padding:15px">Generated Reports</td>
    <td style="padding:15px">{base_path}/{version}/reports/api/v1/{pathv1}/generated?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewsSdwanNocApiViewGetSdwanWanPolicyCompliance(period, resultOrder, count, callback)</td>
    <td style="padding:15px">SDWAN DPS policy compliance report</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-mon-api/external/noc/reports/wan/policy-compliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1BgpNeighbor(device, limit, marker, callback)</td>
    <td style="padding:15px">List BGP neighbor Information</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/bgp/neighbor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1BgpNeighborDetail(device, address, callback)</td>
    <td style="padding:15px">Get BGP neighbor detailed information</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/bgp/neighbor/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRoutingV1BgpNeighborReset(device, address, callback)</td>
    <td style="padding:15px">Reset/clear BGP neighbor session</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/bgp/neighbor/reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1BgpNeighborRouteLearned(device, address, limit, marker, callback)</td>
    <td style="padding:15px">List of routes learned form a BGP neighbor</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/bgp/neighbor/route/learned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1BgpNeighborRouteAdvertised(device, address, limit, marker, callback)</td>
    <td style="padding:15px">List of routes advertised to a BGP neighbor</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/bgp/neighbor/route/advertised?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1OspfNeighbor(device, limit, marker, callback)</td>
    <td style="padding:15px">List OSPF neighbor Information</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/ospf/neighbor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1RipNeighbor(device, limit, marker, callback)</td>
    <td style="padding:15px">List RIP neighbors</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/rip/neighbor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1RipNeighborRoute(device, address, limit, marker, callback)</td>
    <td style="padding:15px">List of routes learned from a RIP neighbor</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/rip/neighbor/route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1BgpRoute(device, limit, marker, callback)</td>
    <td style="padding:15px">List BGP routes</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/bgp/route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1RipRoute(device, limit, marker, callback)</td>
    <td style="padding:15px">List RIP routes</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/rip/route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1Route(device, api, limit, marker, callback)</td>
    <td style="padding:15px">Get routes</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV0Route(device, callback)</td>
    <td style="padding:15px">Get legacy routes</td>
    <td style="padding:15px">{base_path}/{version}/routing/v0/route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1OverlayConnection(device, limit, marker, callback)</td>
    <td style="padding:15px">Get information about overlay control connection</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/overlay/connection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRoutingV1OverlayConnectionReset(device, callback)</td>
    <td style="padding:15px">Reset overlay control connection</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/overlay/connection/reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1OverlayInterface(device, limit, marker, callback)</td>
    <td style="padding:15px">List of overlay interfaces (tunnels)</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/overlay/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1OspfInterface(device, limit, marker, callback)</td>
    <td style="padding:15px">List OSPF Interface Information</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/ospf/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1RipInterface(device, limit, marker, callback)</td>
    <td style="padding:15px">List RIP interfaces</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/rip/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1OverlayRouteLearned(device, limit, marker, callback)</td>
    <td style="padding:15px">List of learned routes from overlay</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/overlay/route/learned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1OverlayRouteLearnedBest(device, limit, marker, callback)</td>
    <td style="padding:15px">List of best learned routes from overlay</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/overlay/route/learned/best?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1OverlayRouteAdvertised(device, limit, marker, callback)</td>
    <td style="padding:15px">List of advertised routes to overlay</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/overlay/route/advertised?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1OspfArea(device, limit, marker, callback)</td>
    <td style="padding:15px">List OSPF Area Information</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/ospf/area?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingV1OspfDatabase(device, limit, marker, callback)</td>
    <td style="padding:15px">List OSPF Link State Database Information</td>
    <td style="padding:15px">{base_path}/{version}/routing/v1/ospf/database?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupServiceIdsId1(name, serviceId, callback)</td>
    <td style="padding:15px">Retrieve service_ids by identifier service_id</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/custom_services/{pathv1}/service_ids/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirgroupServiceIdsId1(name, serviceId, serviceIds, callback)</td>
    <td style="padding:15px">Create service_ids by identifier service_id</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/custom_services/{pathv1}/service_ids/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirgroupServiceIdsId1(name, serviceId, serviceIds, callback)</td>
    <td style="padding:15px">Create/Update service_ids by identifier service_id</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/custom_services/{pathv1}/service_ids/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirgroupServiceIdsId1(name, serviceId, callback)</td>
    <td style="padding:15px">Delete service_ids by identifier service_id</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/custom_services/{pathv1}/service_ids/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupServiceIdsId2(name, callback)</td>
    <td style="padding:15px">Retrieve service_ids</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/custom_services/{pathv1}/service_ids/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupCustomServicesId3(name, callback)</td>
    <td style="padding:15px">Retrieve custom_services by identifier name</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/custom_services/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirgroupCustomServicesId2(name, customServices, callback)</td>
    <td style="padding:15px">Create custom_services by identifier name</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/custom_services/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirgroupCustomServicesId2(name, customServices, callback)</td>
    <td style="padding:15px">Create/Update custom_services by identifier name</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/custom_services/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirgroupCustomServicesId2(name, callback)</td>
    <td style="padding:15px">Delete custom_services by identifier name</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/custom_services/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupCustomServicesId4(callback)</td>
    <td style="padding:15px">Retrieve custom_services</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/custom_services/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupDisallowedRolesId5(macAddress, role, callback)</td>
    <td style="padding:15px">Retrieve disallowed_roles by identifier role</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/role_restrictions/disallowed_roles/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirgroupDisallowedRolesId3(macAddress, role, disallowedRoles, callback)</td>
    <td style="padding:15px">Create disallowed_roles by identifier role</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/role_restrictions/disallowed_roles/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirgroupDisallowedRolesId3(macAddress, role, disallowedRoles, callback)</td>
    <td style="padding:15px">Create/Update disallowed_roles by identifier role</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/role_restrictions/disallowed_roles/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirgroupDisallowedRolesId3(macAddress, role, callback)</td>
    <td style="padding:15px">Delete disallowed_roles by identifier role</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/role_restrictions/disallowed_roles/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupDisallowedRolesId6(macAddress, callback)</td>
    <td style="padding:15px">Retrieve disallowed_roles</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/role_restrictions/disallowed_roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupAllowedRolesId7(macAddress, role, callback)</td>
    <td style="padding:15px">Retrieve allowed_roles by identifier role</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/role_restrictions/allowed_roles/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirgroupAllowedRolesId4(macAddress, role, allowedRoles, callback)</td>
    <td style="padding:15px">Create allowed_roles by identifier role</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/role_restrictions/allowed_roles/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirgroupAllowedRolesId4(macAddress, role, allowedRoles, callback)</td>
    <td style="padding:15px">Create/Update allowed_roles by identifier role</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/role_restrictions/allowed_roles/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirgroupAllowedRolesId4(macAddress, role, callback)</td>
    <td style="padding:15px">Delete allowed_roles by identifier role</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/role_restrictions/allowed_roles/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupAllowedRolesId8(macAddress, callback)</td>
    <td style="padding:15px">Retrieve allowed_roles</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/role_restrictions/allowed_roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupRoleRestrictionsId9(macAddress, callback)</td>
    <td style="padding:15px">Retrieve role_restrictions</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/role_restrictions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirgroupRoleRestrictionsId5(macAddress, roleRestrictions, callback)</td>
    <td style="padding:15px">Create role_restrictions</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/role_restrictions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirgroupRoleRestrictionsId5(macAddress, roleRestrictions, callback)</td>
    <td style="padding:15px">Create/Update role_restrictions</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/role_restrictions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirgroupRoleRestrictionsId5(macAddress, callback)</td>
    <td style="padding:15px">Delete role_restrictions</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/role_restrictions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupApListId10(macAddress, serialNumber, callback)</td>
    <td style="padding:15px">Retrieve ap_list by identifier serial_number</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/network_visibility/ap_list/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirgroupApListId6(macAddress, serialNumber, apList, callback)</td>
    <td style="padding:15px">Create ap_list by identifier serial_number</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/network_visibility/ap_list/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirgroupApListId6(macAddress, serialNumber, apList, callback)</td>
    <td style="padding:15px">Create/Update ap_list by identifier serial_number</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/network_visibility/ap_list/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirgroupApListId6(macAddress, serialNumber, callback)</td>
    <td style="padding:15px">Delete ap_list by identifier serial_number</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/network_visibility/ap_list/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupApListId11(macAddress, callback)</td>
    <td style="padding:15px">Retrieve ap_list</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/network_visibility/ap_list/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupNetworkVisibilityId12(macAddress, callback)</td>
    <td style="padding:15px">Retrieve network_visibility</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/network_visibility/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirgroupNetworkVisibilityId7(macAddress, networkVisibility, callback)</td>
    <td style="padding:15px">Create network_visibility</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/network_visibility/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirgroupNetworkVisibilityId7(macAddress, networkVisibility, callback)</td>
    <td style="padding:15px">Create/Update network_visibility</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/network_visibility/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirgroupNetworkVisibilityId7(macAddress, callback)</td>
    <td style="padding:15px">Delete network_visibility</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/network_visibility/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupServersListId13(macAddress, callback)</td>
    <td style="padding:15px">Retrieve servers_list by identifier mac_address</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirgroupServersListId8(macAddress, serversList, callback)</td>
    <td style="padding:15px">Create servers_list by identifier mac_address</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirgroupServersListId8(macAddress, serversList, callback)</td>
    <td style="padding:15px">Create/Update servers_list by identifier mac_address</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirgroupServersListId8(macAddress, callback)</td>
    <td style="padding:15px">Delete servers_list by identifier mac_address</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupServersListId14(limit, offset, sortBy = 'mac', searchName, searchMac, callback)</td>
    <td style="padding:15px">Retrieve servers_list</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/servers/servers_list/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupAirgroupStatusId15(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve airgroup_status</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/general_settings/airgroup_status/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirgroupAirgroupStatusId9(nodeType = 'GLOBAL', nodeId, airgroupStatus, callback)</td>
    <td style="padding:15px">Create airgroup_status</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/general_settings/airgroup_status/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirgroupAirgroupStatusId9(nodeType = 'GLOBAL', nodeId, airgroupStatus, callback)</td>
    <td style="padding:15px">Create/Update airgroup_status</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/general_settings/airgroup_status/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirgroupAirgroupStatusId9(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete airgroup_status</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/general_settings/airgroup_status/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupGeneralSettingsId16(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve general_settings</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/general_settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirgroupGeneralSettingsId10(nodeType = 'GLOBAL', nodeId, generalSettings, callback)</td>
    <td style="padding:15px">Create general_settings</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/general_settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirgroupGeneralSettingsId10(nodeType = 'GLOBAL', nodeId, generalSettings, callback)</td>
    <td style="padding:15px">Create/Update general_settings</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/general_settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirgroupGeneralSettingsId10(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete general_settings</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/general_settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupDisallowedVlansId17(nodeType = 'GLOBAL', nodeId, name, vlanOrRange, callback)</td>
    <td style="padding:15px">Retrieve disallowed_vlans by identifier vlan_or_range</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/vlan_restrictions/disallowed_vlans/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirgroupDisallowedVlansId11(nodeType = 'GLOBAL', nodeId, name, vlanOrRange, disallowedVlans, callback)</td>
    <td style="padding:15px">Create disallowed_vlans by identifier vlan_or_range</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/vlan_restrictions/disallowed_vlans/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirgroupDisallowedVlansId11(nodeType = 'GLOBAL', nodeId, name, vlanOrRange, disallowedVlans, callback)</td>
    <td style="padding:15px">Create/Update disallowed_vlans by identifier vlan_or_range</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/vlan_restrictions/disallowed_vlans/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirgroupDisallowedVlansId11(nodeType = 'GLOBAL', nodeId, name, vlanOrRange, callback)</td>
    <td style="padding:15px">Delete disallowed_vlans by identifier vlan_or_range</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/vlan_restrictions/disallowed_vlans/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupDisallowedVlansId18(nodeType = 'GLOBAL', nodeId, name, callback)</td>
    <td style="padding:15px">Retrieve disallowed_vlans</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/vlan_restrictions/disallowed_vlans/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupAllowedVlansId19(nodeType = 'GLOBAL', nodeId, name, vlanOrRange, callback)</td>
    <td style="padding:15px">Retrieve allowed_vlans by identifier vlan_or_range</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/vlan_restrictions/allowed_vlans/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirgroupAllowedVlansId12(nodeType = 'GLOBAL', nodeId, name, vlanOrRange, allowedVlans, callback)</td>
    <td style="padding:15px">Create allowed_vlans by identifier vlan_or_range</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/vlan_restrictions/allowed_vlans/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirgroupAllowedVlansId12(nodeType = 'GLOBAL', nodeId, name, vlanOrRange, allowedVlans, callback)</td>
    <td style="padding:15px">Create/Update allowed_vlans by identifier vlan_or_range</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/vlan_restrictions/allowed_vlans/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirgroupAllowedVlansId12(nodeType = 'GLOBAL', nodeId, name, vlanOrRange, callback)</td>
    <td style="padding:15px">Delete allowed_vlans by identifier vlan_or_range</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/vlan_restrictions/allowed_vlans/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupAllowedVlansId20(nodeType = 'GLOBAL', nodeId, name, callback)</td>
    <td style="padding:15px">Retrieve allowed_vlans</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/vlan_restrictions/allowed_vlans/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupVlanRestrictionsId21(nodeType = 'GLOBAL', nodeId, name, callback)</td>
    <td style="padding:15px">Retrieve vlan_restrictions</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/vlan_restrictions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirgroupVlanRestrictionsId13(nodeType = 'GLOBAL', nodeId, name, vlanRestrictions, callback)</td>
    <td style="padding:15px">Create vlan_restrictions</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/vlan_restrictions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirgroupVlanRestrictionsId13(nodeType = 'GLOBAL', nodeId, name, vlanRestrictions, callback)</td>
    <td style="padding:15px">Create/Update vlan_restrictions</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/vlan_restrictions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirgroupVlanRestrictionsId13(nodeType = 'GLOBAL', nodeId, name, callback)</td>
    <td style="padding:15px">Delete vlan_restrictions</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/vlan_restrictions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupDisallowedRolesId22(nodeType = 'GLOBAL', nodeId, name, role, callback)</td>
    <td style="padding:15px">Retrieve disallowed_roles by identifier role</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/role_restrictions/disallowed_roles/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirgroupDisallowedRolesId14(nodeType = 'GLOBAL', nodeId, name, role, disallowedRoles, callback)</td>
    <td style="padding:15px">Create disallowed_roles by identifier role</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/role_restrictions/disallowed_roles/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirgroupDisallowedRolesId14(nodeType = 'GLOBAL', nodeId, name, role, disallowedRoles, callback)</td>
    <td style="padding:15px">Create/Update disallowed_roles by identifier role</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/role_restrictions/disallowed_roles/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirgroupDisallowedRolesId14(nodeType = 'GLOBAL', nodeId, name, role, callback)</td>
    <td style="padding:15px">Delete disallowed_roles by identifier role</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/role_restrictions/disallowed_roles/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupDisallowedRolesId23(nodeType = 'GLOBAL', nodeId, name, callback)</td>
    <td style="padding:15px">Retrieve disallowed_roles</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/role_restrictions/disallowed_roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupAllowedRolesId24(nodeType = 'GLOBAL', nodeId, name, role, callback)</td>
    <td style="padding:15px">Retrieve allowed_roles by identifier role</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/role_restrictions/allowed_roles/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirgroupAllowedRolesId15(nodeType = 'GLOBAL', nodeId, name, role, allowedRoles, callback)</td>
    <td style="padding:15px">Create allowed_roles by identifier role</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/role_restrictions/allowed_roles/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirgroupAllowedRolesId15(nodeType = 'GLOBAL', nodeId, name, role, allowedRoles, callback)</td>
    <td style="padding:15px">Create/Update allowed_roles by identifier role</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/role_restrictions/allowed_roles/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirgroupAllowedRolesId15(nodeType = 'GLOBAL', nodeId, name, role, callback)</td>
    <td style="padding:15px">Delete allowed_roles by identifier role</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/role_restrictions/allowed_roles/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupAllowedRolesId25(nodeType = 'GLOBAL', nodeId, name, callback)</td>
    <td style="padding:15px">Retrieve allowed_roles</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/role_restrictions/allowed_roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupRoleRestrictionsId26(nodeType = 'GLOBAL', nodeId, name, callback)</td>
    <td style="padding:15px">Retrieve role_restrictions</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/role_restrictions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirgroupRoleRestrictionsId16(nodeType = 'GLOBAL', nodeId, name, roleRestrictions, callback)</td>
    <td style="padding:15px">Create role_restrictions</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/role_restrictions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirgroupRoleRestrictionsId16(nodeType = 'GLOBAL', nodeId, name, roleRestrictions, callback)</td>
    <td style="padding:15px">Create/Update role_restrictions</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/role_restrictions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirgroupRoleRestrictionsId16(nodeType = 'GLOBAL', nodeId, name, callback)</td>
    <td style="padding:15px">Delete role_restrictions</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/role_restrictions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupServicesId27(nodeType = 'GLOBAL', nodeId, name, callback)</td>
    <td style="padding:15px">Retrieve services by identifier name</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirgroupServicesId17(nodeType = 'GLOBAL', nodeId, name, services, callback)</td>
    <td style="padding:15px">Create services by identifier name</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirgroupServicesId17(nodeType = 'GLOBAL', nodeId, name, services, callback)</td>
    <td style="padding:15px">Create/Update services by identifier name</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirgroupServicesId17(nodeType = 'GLOBAL', nodeId, name, callback)</td>
    <td style="padding:15px">Delete services by identifier name</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupServicesId28(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve services</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/services/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupConfigId29(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve config</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirgroupNodeListId30(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve node_list by identifier node-type node-id</td>
    <td style="padding:15px">{base_path}/{version}/airgroup-config/v2/node_list/{pathv1}/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirmatchSystemId1(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve system</td>
    <td style="padding:15px">{base_path}/{version}/airmatch-config/v1/node_list/{pathv1}/{pathv2}/config/system/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirmatchSystemId1(nodeType = 'GLOBAL', nodeId, system, callback)</td>
    <td style="padding:15px">Create system</td>
    <td style="padding:15px">{base_path}/{version}/airmatch-config/v1/node_list/{pathv1}/{pathv2}/config/system/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirmatchSystemId1(nodeType = 'GLOBAL', nodeId, system, callback)</td>
    <td style="padding:15px">Create/Update system</td>
    <td style="padding:15px">{base_path}/{version}/airmatch-config/v1/node_list/{pathv1}/{pathv2}/config/system/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirmatchSystemId1(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete system</td>
    <td style="padding:15px">{base_path}/{version}/airmatch-config/v1/node_list/{pathv1}/{pathv2}/config/system/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirmatchConfigId2(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve config</td>
    <td style="padding:15px">{base_path}/{version}/airmatch-config/v1/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaAirmatchConfigId2(nodeType = 'GLOBAL', nodeId, config, callback)</td>
    <td style="padding:15px">Create config</td>
    <td style="padding:15px">{base_path}/{version}/airmatch-config/v1/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaAirmatchConfigId2(nodeType = 'GLOBAL', nodeId, config, callback)</td>
    <td style="padding:15px">Create/Update config</td>
    <td style="padding:15px">{base_path}/{version}/airmatch-config/v1/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaAirmatchConfigId2(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete config</td>
    <td style="padding:15px">{base_path}/{version}/airmatch-config/v1/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaAirmatchNodeListId3(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve node_list by identifier node-type node-id</td>
    <td style="padding:15px">{base_path}/{version}/airmatch-config/v1/node_list/{pathv1}/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaIdsIpsSiemNotificationId1(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve siem_notification</td>
    <td style="padding:15px">{base_path}/{version}/ids-ips-config/v1/node_list/{pathv1}/{pathv2}/config/siem_notification/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaIdsIpsSiemNotificationId1(nodeType = 'GLOBAL', nodeId, siemNotification, callback)</td>
    <td style="padding:15px">Create siem_notification</td>
    <td style="padding:15px">{base_path}/{version}/ids-ips-config/v1/node_list/{pathv1}/{pathv2}/config/siem_notification/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaIdsIpsSiemNotificationId1(nodeType = 'GLOBAL', nodeId, siemNotification, callback)</td>
    <td style="padding:15px">Create/Update siem_notification</td>
    <td style="padding:15px">{base_path}/{version}/ids-ips-config/v1/node_list/{pathv1}/{pathv2}/config/siem_notification/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaIdsIpsSiemNotificationId1(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete siem_notification</td>
    <td style="padding:15px">{base_path}/{version}/ids-ips-config/v1/node_list/{pathv1}/{pathv2}/config/siem_notification/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaIdsIpsSiemServersListId2(nodeType = 'GLOBAL', nodeId, siemServerName, callback)</td>
    <td style="padding:15px">Retrieve siem_servers_list by identifier siem_server_name</td>
    <td style="padding:15px">{base_path}/{version}/ids-ips-config/v1/node_list/{pathv1}/{pathv2}/config/siem_servers_list/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaIdsIpsSiemServersListId2(nodeType = 'GLOBAL', nodeId, siemServerName, siemServersList, callback)</td>
    <td style="padding:15px">Create siem_servers_list by identifier siem_server_name</td>
    <td style="padding:15px">{base_path}/{version}/ids-ips-config/v1/node_list/{pathv1}/{pathv2}/config/siem_servers_list/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaIdsIpsSiemServersListId2(nodeType = 'GLOBAL', nodeId, siemServerName, siemServersList, callback)</td>
    <td style="padding:15px">Create/Update siem_servers_list by identifier siem_server_name</td>
    <td style="padding:15px">{base_path}/{version}/ids-ips-config/v1/node_list/{pathv1}/{pathv2}/config/siem_servers_list/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaIdsIpsSiemServersListId2(nodeType = 'GLOBAL', nodeId, siemServerName, callback)</td>
    <td style="padding:15px">Delete siem_servers_list by identifier siem_server_name</td>
    <td style="padding:15px">{base_path}/{version}/ids-ips-config/v1/node_list/{pathv1}/{pathv2}/config/siem_servers_list/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaIdsIpsSiemServersListId3(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve siem_servers_list</td>
    <td style="padding:15px">{base_path}/{version}/ids-ips-config/v1/node_list/{pathv1}/{pathv2}/config/siem_servers_list/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaIdsIpsConfigId4(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve config</td>
    <td style="padding:15px">{base_path}/{version}/ids-ips-config/v1/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaIdsIpsConfigId3(nodeType = 'GLOBAL', nodeId, config, callback)</td>
    <td style="padding:15px">Create config</td>
    <td style="padding:15px">{base_path}/{version}/ids-ips-config/v1/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaIdsIpsConfigId3(nodeType = 'GLOBAL', nodeId, config, callback)</td>
    <td style="padding:15px">Create/Update config</td>
    <td style="padding:15px">{base_path}/{version}/ids-ips-config/v1/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaIdsIpsConfigId3(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete config</td>
    <td style="padding:15px">{base_path}/{version}/ids-ips-config/v1/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaIdsIpsNodeListId5(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve node_list by identifier node-type node-id</td>
    <td style="padding:15px">{base_path}/{version}/ids-ips-config/v1/node_list/{pathv1}/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaIpmsIpRangeId1(nodeType = 'GLOBAL', nodeId, poolName, rangeId, callback)</td>
    <td style="padding:15px">Retrieve ip_range by identifier range_id</td>
    <td style="padding:15px">{base_path}/{version}/ipms-config/v1/node_list/{pathv1}/{pathv2}/config/address_pool/{pathv3}/ip_range/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaIpmsIpRangeId1(nodeType = 'GLOBAL', nodeId, poolName, rangeId, ipRange, callback)</td>
    <td style="padding:15px">Create ip_range by identifier range_id</td>
    <td style="padding:15px">{base_path}/{version}/ipms-config/v1/node_list/{pathv1}/{pathv2}/config/address_pool/{pathv3}/ip_range/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaIpmsIpRangeId1(nodeType = 'GLOBAL', nodeId, poolName, rangeId, ipRange, callback)</td>
    <td style="padding:15px">Create/Update ip_range by identifier range_id</td>
    <td style="padding:15px">{base_path}/{version}/ipms-config/v1/node_list/{pathv1}/{pathv2}/config/address_pool/{pathv3}/ip_range/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaIpmsIpRangeId2(nodeType = 'GLOBAL', nodeId, poolName, callback)</td>
    <td style="padding:15px">Retrieve ip_range</td>
    <td style="padding:15px">{base_path}/{version}/ipms-config/v1/node_list/{pathv1}/{pathv2}/config/address_pool/{pathv3}/ip_range/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaIpmsAddressPoolId3(nodeType = 'GLOBAL', nodeId, poolName, callback)</td>
    <td style="padding:15px">Retrieve address_pool by identifier pool_name</td>
    <td style="padding:15px">{base_path}/{version}/ipms-config/v1/node_list/{pathv1}/{pathv2}/config/address_pool/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaIpmsAddressPoolId2(nodeType = 'GLOBAL', nodeId, poolName, addressPool, oldKey, callback)</td>
    <td style="padding:15px">Create address_pool by identifier pool_name</td>
    <td style="padding:15px">{base_path}/{version}/ipms-config/v1/node_list/{pathv1}/{pathv2}/config/address_pool/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaIpmsAddressPoolId2(nodeType = 'GLOBAL', nodeId, poolName, addressPool, oldKey, callback)</td>
    <td style="padding:15px">Create/Update address_pool by identifier pool_name</td>
    <td style="padding:15px">{base_path}/{version}/ipms-config/v1/node_list/{pathv1}/{pathv2}/config/address_pool/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaIpmsAddressPoolId1(nodeType = 'GLOBAL', nodeId, poolName, callback)</td>
    <td style="padding:15px">Delete address_pool by identifier pool_name</td>
    <td style="padding:15px">{base_path}/{version}/ipms-config/v1/node_list/{pathv1}/{pathv2}/config/address_pool/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaIpmsAddressPoolId4(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve address_pool</td>
    <td style="padding:15px">{base_path}/{version}/ipms-config/v1/node_list/{pathv1}/{pathv2}/config/address_pool/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaIpmsConfigId5(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve config</td>
    <td style="padding:15px">{base_path}/{version}/ipms-config/v1/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaIpmsNodeListId6(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve node_list by identifier node-type node-id</td>
    <td style="padding:15px">{base_path}/{version}/ipms-config/v1/node_list/{pathv1}/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaOverlayWlanGwClusterListId1(nodeType = 'GROUP', nodeId, profile, profileType = 'WIRELESS_PROFILE', clusterRedundancyType = 'PRIMARY', clusterGroupName, callback)</td>
    <td style="padding:15px">Retrieve gw_cluster_list by identifier cluster_redundancy_type cluster_group_name</td>
    <td style="padding:15px">{base_path}/{version}/overlay-wlan-config/v2/node_list/{pathv1}/{pathv2}/config/ssid_cluster/{pathv3}/{pathv4}/gw_cluster_list/{pathv5}/{pathv6}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaOverlayWlanGwClusterListId1(nodeType = 'GROUP', nodeId, profile, profileType = 'WIRELESS_PROFILE', clusterRedundancyType = 'PRIMARY', clusterGroupName, gwClusterList, callback)</td>
    <td style="padding:15px">Create gw_cluster_list by identifier cluster_redundancy_type cluster_group_name</td>
    <td style="padding:15px">{base_path}/{version}/overlay-wlan-config/v2/node_list/{pathv1}/{pathv2}/config/ssid_cluster/{pathv3}/{pathv4}/gw_cluster_list/{pathv5}/{pathv6}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaOverlayWlanGwClusterListId1(nodeType = 'GROUP', nodeId, profile, profileType = 'WIRELESS_PROFILE', clusterRedundancyType = 'PRIMARY', clusterGroupName, gwClusterList, callback)</td>
    <td style="padding:15px">Create/Update gw_cluster_list by identifier cluster_redundancy_type cluster_group_name</td>
    <td style="padding:15px">{base_path}/{version}/overlay-wlan-config/v2/node_list/{pathv1}/{pathv2}/config/ssid_cluster/{pathv3}/{pathv4}/gw_cluster_list/{pathv5}/{pathv6}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaOverlayWlanGwClusterListId1(nodeType = 'GROUP', nodeId, profile, profileType = 'WIRELESS_PROFILE', clusterRedundancyType = 'PRIMARY', clusterGroupName, callback)</td>
    <td style="padding:15px">Delete gw_cluster_list by identifier cluster_redundancy_type cluster_group_name</td>
    <td style="padding:15px">{base_path}/{version}/overlay-wlan-config/v2/node_list/{pathv1}/{pathv2}/config/ssid_cluster/{pathv3}/{pathv4}/gw_cluster_list/{pathv5}/{pathv6}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaOverlayWlanGwClusterListId2(nodeType = 'GROUP', nodeId, profile, profileType = 'WIRELESS_PROFILE', callback)</td>
    <td style="padding:15px">Retrieve gw_cluster_list</td>
    <td style="padding:15px">{base_path}/{version}/overlay-wlan-config/v2/node_list/{pathv1}/{pathv2}/config/ssid_cluster/{pathv3}/{pathv4}/gw_cluster_list/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaOverlayWlanConfigId5(nodeType = 'GROUP', nodeId, callback)</td>
    <td style="padding:15px">Retrieve config</td>
    <td style="padding:15px">{base_path}/{version}/overlay-wlan-config/v2/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaOverlayWlanConfigId3(nodeType = 'GROUP', nodeId, config, callback)</td>
    <td style="padding:15px">Create config</td>
    <td style="padding:15px">{base_path}/{version}/overlay-wlan-config/v2/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaOverlayWlanConfigId3(nodeType = 'GROUP', nodeId, config, callback)</td>
    <td style="padding:15px">Create/Update config</td>
    <td style="padding:15px">{base_path}/{version}/overlay-wlan-config/v2/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaOverlayWlanConfigId3(nodeType = 'GROUP', nodeId, callback)</td>
    <td style="padding:15px">Delete config</td>
    <td style="padding:15px">{base_path}/{version}/overlay-wlan-config/v2/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaOverlayWlanSsidClusterId3(nodeType = 'GROUP', nodeId, profile, profileType = 'WIRELESS_PROFILE', callback)</td>
    <td style="padding:15px">Retrieve ssid_cluster by identifier profile profile_type</td>
    <td style="padding:15px">{base_path}/{version}/overlay-wlan-config/v2/node_list/{pathv1}/{pathv2}/config/ssid_cluster/{pathv3}/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaOverlayWlanSsidClusterId2(nodeType = 'GROUP', nodeId, profile, profileType = 'WIRELESS_PROFILE', ssidCluster, callback)</td>
    <td style="padding:15px">Create ssid_cluster by identifier profile profile_type</td>
    <td style="padding:15px">{base_path}/{version}/overlay-wlan-config/v2/node_list/{pathv1}/{pathv2}/config/ssid_cluster/{pathv3}/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaOverlayWlanSsidClusterId2(nodeType = 'GROUP', nodeId, profile, profileType = 'WIRELESS_PROFILE', ssidCluster, callback)</td>
    <td style="padding:15px">Create/Update ssid_cluster by identifier profile profile_type</td>
    <td style="padding:15px">{base_path}/{version}/overlay-wlan-config/v2/node_list/{pathv1}/{pathv2}/config/ssid_cluster/{pathv3}/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaOverlayWlanSsidClusterId2(nodeType = 'GROUP', nodeId, profile, profileType = 'WIRELESS_PROFILE', callback)</td>
    <td style="padding:15px">Delete ssid_cluster by identifier profile profile_type</td>
    <td style="padding:15px">{base_path}/{version}/overlay-wlan-config/v2/node_list/{pathv1}/{pathv2}/config/ssid_cluster/{pathv3}/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaOverlayWlanSsidClusterId4(nodeType = 'GROUP', nodeId, callback)</td>
    <td style="padding:15px">Retrieve ssid_cluster</td>
    <td style="padding:15px">{base_path}/{version}/overlay-wlan-config/v2/node_list/{pathv1}/{pathv2}/config/ssid_cluster/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaOverlayWlanNodeListId6(nodeType = 'GROUP', nodeId, callback)</td>
    <td style="padding:15px">Retrieve node_list by identifier node-type node-id</td>
    <td style="padding:15px">{base_path}/{version}/overlay-wlan-config/v2/node_list/{pathv1}/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanAdminStatusId21(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve admin-status</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/sdwan-global/admin-status/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanTopologyId11(nodeType = 'GLOBAL', nodeId, topology, callback)</td>
    <td style="padding:15px">Create/Update topology</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/sdwan-global/topology/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanTopologyId11(nodeType = 'GLOBAL', nodeId, topology, callback)</td>
    <td style="padding:15px">Create topology</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/sdwan-global/topology/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanTopologyId13(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete topology</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/sdwan-global/topology/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanTopologyId22(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve topology</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/sdwan-global/topology/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanSdwanGlobalId25(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve sdwan-global</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/sdwan-global/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanHubClustersId6(nodeType = 'GLOBAL', nodeId, clusterName, clusterGroup, hubClusters, callback)</td>
    <td style="padding:15px">Create/Update hub-clusters by cluster name and cluster group</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/branch-config/hub-clusters/{pathv3}/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanHubClustersId6(nodeType = 'GLOBAL', nodeId, clusterName, clusterGroup, hubClusters, callback)</td>
    <td style="padding:15px">Create by hub-clusters by cluster name and cluster group</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/branch-config/hub-clusters/{pathv3}/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanHubClustersId8(nodeType = 'GLOBAL', nodeId, clusterName, clusterGroup, callback)</td>
    <td style="padding:15px">Delete by hub-clusters by cluster name and cluster group</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/branch-config/hub-clusters/{pathv3}/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanHubClustersId15(nodeType = 'GLOBAL', nodeId, clusterName, clusterGroup, callback)</td>
    <td style="padding:15px">Retrieve by hub-clusters by cluster name and cluster group</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/branch-config/hub-clusters/{pathv3}/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanHubClustersId16(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve hub-clusters</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/branch-config/hub-clusters/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanHubsId14(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve hubs</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/branch-config/hubs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanHubsId7(nodeType = 'GLOBAL', nodeId, identifier, callback)</td>
    <td style="padding:15px">Delete hubs by device serial number</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/branch-config/hubs/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanHubsId13(nodeType = 'GLOBAL', nodeId, identifier, callback)</td>
    <td style="padding:15px">Retrieve hubs by device serial number</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/branch-config/hubs/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanBranchConfigId7(nodeType = 'GLOBAL', nodeId, branchConfig, callback)</td>
    <td style="padding:15px">Create/Update branch-config</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/branch-config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanBranchConfigId7(nodeType = 'GLOBAL', nodeId, branchConfig, callback)</td>
    <td style="padding:15px">Create branch-config</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/branch-config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanBranchConfigId9(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete branch-config</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/branch-config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanBranchConfigId17(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve branch-config</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/branch-config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanLoadBalanceOrchestrationId18(nodeType = 'GLOBAL', nodeId, loadBalanceOrchestration, callback)</td>
    <td style="padding:15px">Create/Update load-balance-orchestration</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/load-balance-orchestration/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanLoadBalanceOrchestrationId18(nodeType = 'GLOBAL', nodeId, loadBalanceOrchestration, callback)</td>
    <td style="padding:15px">Create load-balance-orchestration</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/load-balance-orchestration/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanLoadBalanceOrchestrationId21(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete load-balance-orchestration</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/load-balance-orchestration/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanLoadBalanceOrchestrationId31(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve load-balance-orchestration</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/load-balance-orchestration/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanRoutePolicyId36(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve route-policy</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanAsNumberId13(nodeType = 'GLOBAL', nodeId, asNumber, callback)</td>
    <td style="padding:15px">Create/Update as-number</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/as-number/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanAsNumberId13(nodeType = 'GLOBAL', nodeId, asNumber, callback)</td>
    <td style="padding:15px">Create as-number</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/as-number/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanAsNumberId16(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete as-number</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/as-number/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanAsNumberId26(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve as-number</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/as-number/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanHubId17(nodeType = 'GLOBAL', nodeId, hub, callback)</td>
    <td style="padding:15px">Create/Update hub</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/hub/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanHubId17(nodeType = 'GLOBAL', nodeId, hub, callback)</td>
    <td style="padding:15px">Create hub</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/hub/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanHubId20(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete hub</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/hub/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanHubId30(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve hub</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/hub/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanBestPathComputationId16(nodeType = 'GLOBAL', nodeId, bestPathComputation, callback)</td>
    <td style="padding:15px">Create/Update best-path-computation</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/hub/best-path-computation/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanBestPathComputationId16(nodeType = 'GLOBAL', nodeId, bestPathComputation, callback)</td>
    <td style="padding:15px">Create best-path-computation</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/hub/best-path-computation/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanBestPathComputationId19(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete best-path-computation</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/hub/best-path-computation/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanBestPathComputationId29(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve best-path-computation</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/hub/best-path-computation/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanGracefulRestartId15(nodeType = 'GLOBAL', nodeId, gracefulRestart, callback)</td>
    <td style="padding:15px">Create/Update Global graceful restart timer</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/graceful-restart/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanGracefulRestartId15(nodeType = 'GLOBAL', nodeId, gracefulRestart, callback)</td>
    <td style="padding:15px">Create Global graceful restart timer</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/graceful-restart/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanGracefulRestartId18(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete Global graceful restart timer</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/graceful-restart/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanGracefulRestartId28(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve Global graceful restart timer</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/graceful-restart/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanTimerId14(nodeType = 'GLOBAL', nodeId, timer, callback)</td>
    <td style="padding:15px">Create/Update timer</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/graceful-restart/timer/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanTimerId14(nodeType = 'GLOBAL', nodeId, timer, callback)</td>
    <td style="padding:15px">Create timer</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/graceful-restart/timer/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanTimerId17(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete timer</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/graceful-restart/timer/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanTimerId27(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve timer</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/graceful-restart/timer/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanHubMeshId42(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve hub-mesh</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/mesh-policy/hub-mesh/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanHubMeshId24(nodeType = 'GLOBAL', nodeId, label, hubMesh, callback)</td>
    <td style="padding:15px">Create/Update hub-mesh by label</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/mesh-policy/hub-mesh/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanHubMeshId24(nodeType = 'GLOBAL', nodeId, label, hubMesh, callback)</td>
    <td style="padding:15px">Create hub-mesh by label</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/mesh-policy/hub-mesh/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanHubMeshId28(nodeType = 'GLOBAL', nodeId, label, callback)</td>
    <td style="padding:15px">Delete hub-mesh by label</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/mesh-policy/hub-mesh/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanHubMeshId41(nodeType = 'GLOBAL', nodeId, label, callback)</td>
    <td style="padding:15px">Retrieve hub-mesh by label</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/mesh-policy/hub-mesh/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanHubGroupsId40(nodeType = 'GLOBAL', nodeId, label, callback)</td>
    <td style="padding:15px">Retrieve hub-groups</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/mesh-policy/hub-mesh/{pathv3}/hub-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanHubGroupsId23(nodeType = 'GLOBAL', nodeId, label, name, hubGroups, callback)</td>
    <td style="padding:15px">Create/Update hub-groups by name</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/mesh-policy/hub-mesh/{pathv3}/hub-groups/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanHubGroupsId23(nodeType = 'GLOBAL', nodeId, label, name, hubGroups, callback)</td>
    <td style="padding:15px">Create hub-groups by name</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/mesh-policy/hub-mesh/{pathv3}/hub-groups/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanHubGroupsId27(nodeType = 'GLOBAL', nodeId, label, name, callback)</td>
    <td style="padding:15px">Delete hub-groups by name</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/mesh-policy/hub-mesh/{pathv3}/hub-groups/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanHubGroupsId39(nodeType = 'GLOBAL', nodeId, label, name, callback)</td>
    <td style="padding:15px">Retrieve hub-groups by name</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/mesh-policy/hub-mesh/{pathv3}/hub-groups/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanTunnelPolicyId22(nodeType = 'GLOBAL', nodeId, tunnelPolicy, callback)</td>
    <td style="padding:15px">Create/Update tunnel-policy</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/tunnel-policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanTunnelPolicyId22(nodeType = 'GLOBAL', nodeId, tunnelPolicy, callback)</td>
    <td style="padding:15px">Create tunnel-policy</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/tunnel-policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanTunnelPolicyId26(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete tunnel-policy</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/tunnel-policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanTunnelPolicyId38(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve tunnel-policy</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/tunnel-policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanRekeyIntervalId21(nodeType = 'GLOBAL', nodeId, rekeyInterval, callback)</td>
    <td style="padding:15px">Create/Update rekey-interval</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/tunnel-policy/rekey-interval/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanRekeyIntervalId21(nodeType = 'GLOBAL', nodeId, rekeyInterval, callback)</td>
    <td style="padding:15px">Create rekey-interval</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/tunnel-policy/rekey-interval/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanRekeyIntervalId25(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete rekey-interval</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/tunnel-policy/rekey-interval/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanRekeyIntervalId37(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve rekey-interval</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/tunnel-policy/rekey-interval/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanBranchMeshId4(label, callback)</td>
    <td style="padding:15px">Retrieve branch-mesh by label</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/branch-mesh/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanBranchMeshId5(limit, offset, lastIndex, searchName, callback)</td>
    <td style="padding:15px">Retrieve branch-mesh</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/branch-mesh/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanBranchDevicesId1(label, identifier, branchDevices, callback)</td>
    <td style="padding:15px">Create/Update branch-devices by device serial number</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/branch-mesh/{pathv1}/config/branch-devices/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanBranchDevicesId1(label, identifier, branchDevices, callback)</td>
    <td style="padding:15px">Create branch-devices by device serial number</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/branch-mesh/{pathv1}/config/branch-devices/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanBranchDevicesId1(label, identifier, callback)</td>
    <td style="padding:15px">Delete branch-devices by device serial number</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/branch-mesh/{pathv1}/config/branch-devices/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanBranchDevicesId1(label, identifier, callback)</td>
    <td style="padding:15px">Retrieve branch-devices by device serial number</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/branch-mesh/{pathv1}/config/branch-devices/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanConfigId2(label, config, callback)</td>
    <td style="padding:15px">Create/Update branch-mesh config</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/branch-mesh/{pathv1}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanConfigId2(label, config, callback)</td>
    <td style="padding:15px">Create branch-mesh config</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/branch-mesh/{pathv1}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanConfigId2(label, callback)</td>
    <td style="padding:15px">Delete branch-mesh config</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/branch-mesh/{pathv1}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanConfigId3(label, callback)</td>
    <td style="padding:15px">Retrieve branch-mesh config</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/branch-mesh/{pathv1}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanBranchMeshUiId6(limit, offset, lastIndex, searchName, callback)</td>
    <td style="padding:15px">Retrieve branch-mesh-ui</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/branch-mesh-ui/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanBranchDevicesId2(label, callback)</td>
    <td style="padding:15px">Retrieve branch-devices</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/branch-mesh/{pathv1}/config/branch-devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanHubClustersId19(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve hub-clusters</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/microbranch-dc-cluster/hub-clusters/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanHubClustersId8(nodeType = 'GLOBAL', nodeId, clusterName, clusterGroup, hubClusters, callback)</td>
    <td style="padding:15px">Create/Update hub-clusters by identifier cluster-name cluster-group</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/microbranch-dc-cluster/hub-clusters/{pathv3}/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanHubClustersId8(nodeType = 'GLOBAL', nodeId, clusterName, clusterGroup, hubClusters, callback)</td>
    <td style="padding:15px">Create hub-clusters by identifier cluster-name cluster-group</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/microbranch-dc-cluster/hub-clusters/{pathv3}/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanHubClustersId10(nodeType = 'GLOBAL', nodeId, clusterName, clusterGroup, callback)</td>
    <td style="padding:15px">Delete hub-clusters by identifier cluster-name cluster-group</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/microbranch-dc-cluster/hub-clusters/{pathv3}/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanHubClustersId18(nodeType = 'GLOBAL', nodeId, clusterName, clusterGroup, callback)</td>
    <td style="padding:15px">Retrieve hub-clusters by identifier cluster-name cluster-group</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/microbranch-dc-cluster/hub-clusters/{pathv3}/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanMicrobranchDcClusterId9(nodeType = 'GLOBAL', nodeId, microbranchDcCluster, callback)</td>
    <td style="padding:15px">Create/Update microbranch-dc-cluster</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/microbranch-dc-cluster/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanMicrobranchDcClusterId9(nodeType = 'GLOBAL', nodeId, microbranchDcCluster, callback)</td>
    <td style="padding:15px">Create microbranch-dc-cluster</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/microbranch-dc-cluster/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanMicrobranchDcClusterId11(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete microbranch-dc-cluster</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/microbranch-dc-cluster/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanMicrobranchDcClusterId20(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve microbranch-dc-cluster</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/microbranch-dc-cluster/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanTransitId3(nodeType = 'GLOBAL', nodeId, transit, callback)</td>
    <td style="padding:15px">Create/Update transit</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/hub-config/transit/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanTransitId3(nodeType = 'GLOBAL', nodeId, transit, callback)</td>
    <td style="padding:15px">Create transit</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/hub-config/transit/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanTransitId3(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete transit</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/hub-config/transit/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanTransitId7(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve transit</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/hub-config/transit/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanHubConfigId12(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve hub-config</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/hub-config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanConfigId44(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve config</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanNodeListId45(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve node_list by identifier node-type node-id</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanHubAggregatesId11(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve hub-aggregates</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/hub-config/hub-aggregates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanAggregatesId9(nodeType = 'GLOBAL', nodeId, segment, callback)</td>
    <td style="padding:15px">Retrieve aggregates for hub-aggregates</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/hub-config/hub-aggregates/{pathv3}/aggregates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanAggregatesId4(nodeType = 'GLOBAL', nodeId, segment, prefix, aggregates, callback)</td>
    <td style="padding:15px">Create/Update DC aggregate routes by prefix for given network segment</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/hub-config/hub-aggregates/{pathv3}/aggregates/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanAggregatesId4(nodeType = 'GLOBAL', nodeId, segment, prefix, aggregates, callback)</td>
    <td style="padding:15px">Create DC aggregate routes by prefix for given network segment</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/hub-config/hub-aggregates/{pathv3}/aggregates/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanAggregatesId4(nodeType = 'GLOBAL', nodeId, segment, prefix, callback)</td>
    <td style="padding:15px">Delete DC aggregate routes by prefix for given network segment</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/hub-config/hub-aggregates/{pathv3}/aggregates/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanAggregatesId8(nodeType = 'GLOBAL', nodeId, segment, prefix, callback)</td>
    <td style="padding:15px">Retrieve DC aggregate routes by prefix for given network segment</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/hub-config/hub-aggregates/{pathv3}/aggregates/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanHubAggregatesId5(nodeType = 'GLOBAL', nodeId, segment, hubAggregates, callback)</td>
    <td style="padding:15px">Create/Update DC aggregate routes for given network segment</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/hub-config/hub-aggregates/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanHubAggregatesId5(nodeType = 'GLOBAL', nodeId, segment, hubAggregates, callback)</td>
    <td style="padding:15px">Create DC aggregate routes for given network segment</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/hub-config/hub-aggregates/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanHubAggregatesId5(nodeType = 'GLOBAL', nodeId, segment, callback)</td>
    <td style="padding:15px">Delete DC aggregate routes for given network segment</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/hub-config/hub-aggregates/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanHubAggregatesId10(nodeType = 'GLOBAL', nodeId, segment, callback)</td>
    <td style="padding:15px">Retrieve DC aggregate routes for given network segment</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/hub-config/hub-aggregates/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanMeshPolicyId29(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete mesh-policy</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/mesh-policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanMeshPolicyId43(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve mesh-policy</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/mesh-policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanNetworkSegmentPolicyId24(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve network-segment-policy</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/sdwan-global/network-segment-policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanNetworkSegmentPolicyId12(nodeType = 'GLOBAL', nodeId, name, networkSegmentPolicy, callback)</td>
    <td style="padding:15px">Create/Update network-segment-policy by segment name</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/sdwan-global/network-segment-policy/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanNetworkSegmentPolicyId12(nodeType = 'GLOBAL', nodeId, name, networkSegmentPolicy, callback)</td>
    <td style="padding:15px">Create network-segment-policy by segment name</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/sdwan-global/network-segment-policy/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanNetworkSegmentPolicyId14(nodeType = 'GLOBAL', nodeId, name, callback)</td>
    <td style="padding:15px">Delete network-segment-policy by segment name</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/sdwan-global/network-segment-policy/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanNetworkSegmentPolicyId23(nodeType = 'GLOBAL', nodeId, name, callback)</td>
    <td style="padding:15px">Retrieve network-segment-policy by segment name</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/sdwan-global/network-segment-policy/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanAggregatesId33(nodeType = 'GLOBAL', nodeId, segment, callback)</td>
    <td style="padding:15px">Retrieve aggregates for branch-aggregates</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/branch-aggregates/{pathv3}/aggregates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanBranchAggregatesId35(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve branch-aggregates</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/branch-aggregates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanAggregatesId19(nodeType = 'GLOBAL', nodeId, segment, prefix, aggregates, callback)</td>
    <td style="padding:15px">Create/Update global branch aggregate routes by prefix for given network segment</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/branch-aggregates/{pathv3}/aggregates/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanAggregatesId19(nodeType = 'GLOBAL', nodeId, segment, prefix, aggregates, callback)</td>
    <td style="padding:15px">Create global branch aggregate routes by prefix for given network segment</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/branch-aggregates/{pathv3}/aggregates/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanAggregatesId22(nodeType = 'GLOBAL', nodeId, segment, prefix, callback)</td>
    <td style="padding:15px">Delete global branch aggregate routes by prefix for given network segment</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/branch-aggregates/{pathv3}/aggregates/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanAggregatesId32(nodeType = 'GLOBAL', nodeId, segment, prefix, callback)</td>
    <td style="padding:15px">Retrieve global branch aggregate routes by prefix for given network segment</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/branch-aggregates/{pathv3}/aggregates/{pathv4}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaSdwanBranchAggregatesId20(nodeType = 'GLOBAL', nodeId, segment, branchAggregates, callback)</td>
    <td style="padding:15px">Create/Update global branch aggregate routes for given network segment</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/branch-aggregates/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaSdwanBranchAggregatesId20(nodeType = 'GLOBAL', nodeId, segment, branchAggregates, callback)</td>
    <td style="padding:15px">Create global branch aggregate routes for given network segment</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/branch-aggregates/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaSdwanBranchAggregatesId23(nodeType = 'GLOBAL', nodeId, segment, callback)</td>
    <td style="padding:15px">Delete global branch aggregate routes for given network segment</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/branch-aggregates/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaSdwanBranchAggregatesId34(nodeType = 'GLOBAL', nodeId, segment, callback)</td>
    <td style="padding:15px">Retrieve global branch aggregate routes for given network segment</td>
    <td style="padding:15px">{base_path}/{version}/sdwan-config/v1/node_list/{pathv1}/{pathv2}/config/route-policy/branch-aggregates/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaUccSkype4bId1(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve skype4b</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/skype4b/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaUccSkype4bId1(nodeType = 'GLOBAL', nodeId, skype4b, callback)</td>
    <td style="padding:15px">Create skype4b</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/skype4b/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaUccSkype4bId1(nodeType = 'GLOBAL', nodeId, skype4b, callback)</td>
    <td style="padding:15px">Create/Update skype4b</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/skype4b/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaUccSkype4bId1(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete skype4b</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/skype4b/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaUccDnsPatternsId2(nodeType = 'GLOBAL', nodeId, dnsPattern, callback)</td>
    <td style="padding:15px">Retrieve dns_patterns by identifier dns_pattern</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/wifi_calling/dns_patterns/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaUccDnsPatternsId2(nodeType = 'GLOBAL', nodeId, dnsPattern, dnsPatterns, callback)</td>
    <td style="padding:15px">Create dns_patterns by identifier dns_pattern</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/wifi_calling/dns_patterns/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaUccDnsPatternsId2(nodeType = 'GLOBAL', nodeId, dnsPattern, dnsPatterns, callback)</td>
    <td style="padding:15px">Create/Update dns_patterns by identifier dns_pattern</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/wifi_calling/dns_patterns/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaUccDnsPatternsId2(nodeType = 'GLOBAL', nodeId, dnsPattern, callback)</td>
    <td style="padding:15px">Delete dns_patterns by identifier dns_pattern</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/wifi_calling/dns_patterns/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaUccDnsPatternsId3(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve dns_patterns</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/wifi_calling/dns_patterns/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaUccWifiCallingId4(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve wifi_calling</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/wifi_calling/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaUccWifiCallingId3(nodeType = 'GLOBAL', nodeId, wifiCalling, callback)</td>
    <td style="padding:15px">Create wifi_calling</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/wifi_calling/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaUccWifiCallingId3(nodeType = 'GLOBAL', nodeId, wifiCalling, callback)</td>
    <td style="padding:15px">Create/Update wifi_calling</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/wifi_calling/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaUccWifiCallingId3(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete wifi_calling</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/wifi_calling/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaUccSipId5(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve sip</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/sip/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaUccSipId4(nodeType = 'GLOBAL', nodeId, sip, callback)</td>
    <td style="padding:15px">Create sip</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/sip/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaUccSipId4(nodeType = 'GLOBAL', nodeId, sip, callback)</td>
    <td style="padding:15px">Create/Update sip</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/sip/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaUccSipId4(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete sip</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/sip/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaUccUccAlgId6(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve ucc_alg</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaUccUccAlgId5(nodeType = 'GLOBAL', nodeId, uccAlg, callback)</td>
    <td style="padding:15px">Create ucc_alg</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaUccUccAlgId5(nodeType = 'GLOBAL', nodeId, uccAlg, callback)</td>
    <td style="padding:15px">Create/Update ucc_alg</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaUccUccAlgId5(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete ucc_alg</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_alg/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaUccActivateId7(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve activate</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_settings/activate/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaUccActivateId6(nodeType = 'GLOBAL', nodeId, activate, callback)</td>
    <td style="padding:15px">Create activate</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_settings/activate/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaUccActivateId6(nodeType = 'GLOBAL', nodeId, activate, callback)</td>
    <td style="padding:15px">Create/Update activate</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_settings/activate/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaUccActivateId6(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete activate</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_settings/activate/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaUccUccSettingsId8(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve ucc_settings</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaUccUccSettingsId7(nodeType = 'GLOBAL', nodeId, uccSettings, callback)</td>
    <td style="padding:15px">Create ucc_settings</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaUccUccSettingsId7(nodeType = 'GLOBAL', nodeId, uccSettings, callback)</td>
    <td style="padding:15px">Create/Update ucc_settings</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaUccUccSettingsId7(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete ucc_settings</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/ucc_settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaUccConfigId9(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve config</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postArubaUccConfigId8(nodeType = 'GLOBAL', nodeId, config, callback)</td>
    <td style="padding:15px">Create config</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putArubaUccConfigId8(nodeType = 'GLOBAL', nodeId, config, callback)</td>
    <td style="padding:15px">Create/Update config</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaUccConfigId8(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Delete config</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaUccNodeListId10(nodeType = 'GLOBAL', nodeId, callback)</td>
    <td style="padding:15px">Retrieve node_list by identifier node-type node-id</td>
    <td style="padding:15px">{base_path}/{version}/ucc-config/v1/node_list/{pathv1}/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsTopologyExternalApiViewsDisplayTopology(siteId, callback)</td>
    <td style="padding:15px">Get topology details of a site.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsTopologyExternalApiViewsDisplayTopologyDevices(deviceSerial, callback)</td>
    <td style="padding:15px">Get details of a device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsTopologyExternalApiViewsDisplayTopologyEdges(sourceSerial, destSerial, callback)</td>
    <td style="padding:15px">Get details of an edge.</td>
    <td style="padding:15px">{base_path}/{version}/edges/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsTopologyExternalApiViewsDisplayTopologyEdgesV2(sourceSerial, destSerial, callback)</td>
    <td style="padding:15px">Get details of an edge for the selected source and destination.</td>
    <td style="padding:15px">{base_path}/{version}/v2/edges/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsTopologyExternalApiViewsDisplayTopologyUplinks(sourceSerial, uplinkId, callback)</td>
    <td style="padding:15px">Get details of an uplink.</td>
    <td style="padding:15px">{base_path}/{version}/uplinks/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsTopologyExternalApiViewsDisplayGetTunnel(siteId, tunnelMapNames, callback)</td>
    <td style="padding:15px">Get tunnel details.</td>
    <td style="padding:15px">{base_path}/{version}/tunnels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsTopologyExternalApiViewsDisplayTopologyApNeighbors(deviceSerial, callback)</td>
    <td style="padding:15px">Get neighbor details reported by AP via LLDP.</td>
    <td style="padding:15px">{base_path}/{version}/apNeighbors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsTopologyExternalApiViewsDisplayTopologyVlans(siteId, limit, offset, search, callback)</td>
    <td style="padding:15px">Get vlan list of a site.</td>
    <td style="padding:15px">{base_path}/{version}/vlans/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiViewsTopologyExternalApiViewsDisplayTopologyExpiringDevices(siteId, callback)</td>
    <td style="padding:15px">Get list of unreachable devices in a site.</td>
    <td style="padding:15px">{base_path}/{version}/unreachableDevices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTroubleshootingApiGetCommandsList(deviceType, callback)</td>
    <td style="padding:15px">List Troubleshooting Commands</td>
    <td style="padding:15px">{base_path}/{version}/troubleshooting/v1/commands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTroubleshootingApiStartTroubleshoot(serial, body, callback)</td>
    <td style="padding:15px">Start Troubleshooting Session</td>
    <td style="padding:15px">{base_path}/{version}/troubleshooting/v1/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTroubleshootingApiGetTroubleshootOutput(serial, sessionId, callback)</td>
    <td style="padding:15px">Get Troubleshooting Output</td>
    <td style="padding:15px">{base_path}/{version}/troubleshooting/v1/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTroubleshootingApiClearTroubleshootingSession(serial, sessionId, callback)</td>
    <td style="padding:15px">Clear Troubleshooting Session</td>
    <td style="padding:15px">{base_path}/{version}/troubleshooting/v1/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTroubleshootingApiGetSessionId(serial, callback)</td>
    <td style="padding:15px">Get Troubleshooting Session ID</td>
    <td style="padding:15px">{base_path}/{version}/troubleshooting/v1/devices/{pathv1}/session?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTroubleshootingApiExportOutput(serial, sessionId, callback)</td>
    <td style="padding:15px">Export Troubleshooting Output</td>
    <td style="padding:15px">{base_path}/{version}/troubleshooting/v1/devices/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTroubleshootingApiPostRunningConfigBackupSerial(serial, prefix, callback)</td>
    <td style="padding:15px">Initiate Running Config Backup For A Single Device</td>
    <td style="padding:15px">{base_path}/{version}/troubleshooting/v1/running-config-backup/serial/{pathv1}/prefix/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTroubleshootingApiGetNamedBackupsForSerialPrefix(serial, prefix, callback)</td>
    <td style="padding:15px">Get list of backups associated with the device serial with the given prefix</td>
    <td style="padding:15px">{base_path}/{version}/troubleshooting/v1/running-config-backup/serial/{pathv1}/prefix/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTroubleshootingApiPostRunningConfigBackupGroupName(groupName, prefix, callback)</td>
    <td style="padding:15px">Initiate Running Config Backup For All Devices In A Group</td>
    <td style="padding:15px">{base_path}/{version}/troubleshooting/v1/running-config-backup/group_name/{pathv1}/prefix/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTroubleshootingApiGetNamedBackupsForSerial(serial, callback)</td>
    <td style="padding:15px">Get list of backups associated with the device serial</td>
    <td style="padding:15px">{base_path}/{version}/troubleshooting/v1/running-config-backup/serial/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTroubleshootingApiGetRunningConfigBackupWithName(name, callback)</td>
    <td style="padding:15px">Fetch the backup stored against the given name</td>
    <td style="padding:15px">{base_path}/{version}/troubleshooting/v1/running-config-backup/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ucDshbViewsGetUcClients(label, startTime, endTime, callback)</td>
    <td style="padding:15px">Fetch number of Clients</td>
    <td style="padding:15px">{base_path}/{version}/v1/client/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ucDshbViewsGetUcCountBySt(label, startTime, endTime, callback)</td>
    <td style="padding:15px">Fetch session counts based on session type</td>
    <td style="padding:15px">{base_path}/{version}/v1/session/trend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ucDshbViewsGetSessionCountByProtocol(label, startTime, endTime, callback)</td>
    <td style="padding:15px">Fetch session counts based on alg name</td>
    <td style="padding:15px">{base_path}/{version}/v1/session/count/alg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ucDshbViewsGetUcSqBySt(label, startTime, endTime, callback)</td>
    <td style="padding:15px">Fetch session quality based on session type</td>
    <td style="padding:15px">{base_path}/{version}/v1/session/quality/type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ucDshbViewsGetUcSqBySsid(label, startTime, endTime, callback)</td>
    <td style="padding:15px">Fetch session quality based on ssid</td>
    <td style="padding:15px">{base_path}/{version}/v1/session/quality/ssid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ucDshbViewsGetUcInsightsCount(label, startTime, endTime, callback)</td>
    <td style="padding:15px">Fetch number of insights for a day</td>
    <td style="padding:15px">{base_path}/{version}/v1/insights/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ucDshbViewsGetUcCdrs(label, startTime, endTime, offset, limit, callback)</td>
    <td style="padding:15px">Fetch list of CDRs</td>
    <td style="padding:15px">{base_path}/{version}/v1/cdr/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ucDshbViewsExportUcCdrs(label, startTime, endTime, callback)</td>
    <td style="padding:15px">Export list of CDRs</td>
    <td style="padding:15px">{base_path}/{version}/v1/cdr/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ucDshbViewsGetUcSkypeElb(callback)</td>
    <td style="padding:15px">Fetch skype server central termination URL</td>
    <td style="padding:15px">{base_path}/{version}/v1/SkypeCentralURL?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiUserCreateBulkUsersAccount(params, callback)</td>
    <td style="padding:15px">Create multiple users account. The max no of accounts that can be created at once is 10.</td>
    <td style="padding:15px">{base_path}/{version}/platform/rbac/v1/bulk_users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiUserUpdateBulkUsersAccount(params, callback)</td>
    <td style="padding:15px">Update multiple users account. The max no of accounts that can be updated at once is 10.Providing r</td>
    <td style="padding:15px">{base_path}/{version}/platform/rbac/v1/bulk_users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiUserDeleteBulkUsersAccount(params, callback)</td>
    <td style="padding:15px">Delete multiple users account. The max no of accounts that can be deleted at once is 10.</td>
    <td style="padding:15px">{base_path}/{version}/platform/rbac/v1/bulk_users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiUserBulkUsersGetCookieStatus(cookieName, callback)</td>
    <td style="padding:15px">Get task status</td>
    <td style="padding:15px">{base_path}/{version}/platform/rbac/v1/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRoleGetRoles(limit, offset, orderBy = '+rolename', appName, callback)</td>
    <td style="padding:15px">Get list of all roles</td>
    <td style="padding:15px">{base_path}/{version}/platform/rbac/v1/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRoleGetRole(rolename, appName, callback)</td>
    <td style="padding:15px">Get Role details</td>
    <td style="padding:15px">{base_path}/{version}/platform/rbac/v1/apps/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRoleDeleteRole(rolename, appName, callback)</td>
    <td style="padding:15px">Delete a  role</td>
    <td style="padding:15px">{base_path}/{version}/platform/rbac/v1/apps/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRoleUpdateRole(rolename, appName, params, callback)</td>
    <td style="padding:15px">Update a role</td>
    <td style="padding:15px">{base_path}/{version}/platform/rbac/v1/apps/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiRoleCreateRole(params, appName, callback)</td>
    <td style="padding:15px">Create a role in an app</td>
    <td style="padding:15px">{base_path}/{version}/platform/rbac/v1/apps/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVisualrfApiV1ClientLocationMacaddr(macaddr, offset, limit, units = 'METERS', callback)</td>
    <td style="padding:15px">Get location of a specific client</td>
    <td style="padding:15px">{base_path}/{version}/visualrf_api/v1/client_location/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVisualrfApiV1FloorFloorIdClientLocation(floorId, offset, limit, units = 'METERS', callback)</td>
    <td style="padding:15px">Get a specific floor and location of all its clients</td>
    <td style="padding:15px">{base_path}/{version}/visualrf_api/v1/floor/{pathv1}/client_location?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVisualrfApiV1RogueLocationMacaddr(macaddr, offset, limit, units = 'METERS', callback)</td>
    <td style="padding:15px">Get location of a specific rogue access point</td>
    <td style="padding:15px">{base_path}/{version}/visualrf_api/v1/rogue_location/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVisualrfApiV1FloorFloorIdRogueLocation(floorId, offset, limit, units = 'METERS', callback)</td>
    <td style="padding:15px">Get a specific floor and location of all its rogue access points</td>
    <td style="padding:15px">{base_path}/{version}/visualrf_api/v1/floor/{pathv1}/rogue_location?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVisualrfApiV1Campus(offset, limit, callback)</td>
    <td style="padding:15px">Get list of all campuses</td>
    <td style="padding:15px">{base_path}/{version}/visualrf_api/v1/campus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVisualrfApiV1CampusCampusId(campusId, offset, limit, callback)</td>
    <td style="padding:15px">Get a specific campus and its buildings</td>
    <td style="padding:15px">{base_path}/{version}/visualrf_api/v1/campus/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVisualrfApiV1BuildingBuildingId(buildingId, offset, limit, units = 'METERS', callback)</td>
    <td style="padding:15px">Get a specific building and its floors</td>
    <td style="padding:15px">{base_path}/{version}/visualrf_api/v1/building/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVisualrfApiV1FloorFloorId(floorId, offset, limit, units = 'METERS', callback)</td>
    <td style="padding:15px">Get details of a specific floor</td>
    <td style="padding:15px">{base_path}/{version}/visualrf_api/v1/floor/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVisualrfApiV1FloorFloorIdImage(floorId, offset, limit, callback)</td>
    <td style="padding:15px">Get background image of a specific floor</td>
    <td style="padding:15px">{base_path}/{version}/visualrf_api/v1/floor/{pathv1}/image?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVisualrfApiV1FloorFloorIdAccessPointLocation(floorId, offset, limit, units = 'METERS', callback)</td>
    <td style="padding:15px">Get a specific floor and location of all its access points</td>
    <td style="padding:15px">{base_path}/{version}/visualrf_api/v1/floor/{pathv1}/access_point_location?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVisualrfApiV1AccessPointLocationApId(apId, offset, limit, units = 'METERS', callback)</td>
    <td style="padding:15px">Get location of a specific access point</td>
    <td style="padding:15px">{base_path}/{version}/visualrf_api/v1/access_point_location/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVisualrfApiV1RestoreSites(file, callback)</td>
    <td style="padding:15px">create floorplans using zip file (supported files are zip and .esx)</td>
    <td style="padding:15px">{base_path}/{version}/visualrf_api/v1/restore_sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVisualrfApiV1RestoreSitesStatus(callback)</td>
    <td style="padding:15px">get last import operation information</td>
    <td style="padding:15px">{base_path}/{version}/visualrf_api/v1/restore_sites/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVisualrfApiV1Anonymization(callback)</td>
    <td style="padding:15px">Get status of anonymization</td>
    <td style="padding:15px">{base_path}/{version}/visualrf_api/v1/anonymization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVisualrfApiV1Anonymization(enableAnonymization, callback)</td>
    <td style="padding:15px">Enable anonymization</td>
    <td style="padding:15px">{base_path}/{version}/visualrf_api/v1/anonymization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVisualrfApiV1Anonymization(callback)</td>
    <td style="padding:15px">Disable anonymization</td>
    <td style="padding:15px">{base_path}/{version}/visualrf_api/v1/anonymization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
