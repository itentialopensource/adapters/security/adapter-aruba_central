## Authenticating Aruba Central Adapter 

This document will go through the steps for authenticating the Aruba Central adapter with Static Token Authentication and Multi Step Authentication. Properly configuring the properties for an adapter in IAP is critical for getting the adapter online. You can read more about adapter authentication <a href="https://docs.itential.com/opensource/docs/authentication" target="_blank">HERE</a>. 

Companies periodically change authentication methods to provide better security. As this happens this section should be updated and contributed/merge back into the adapter repository.

### Static Token Authentication
The Aruba Central adapter requires Static Token Authentication. If you change authentication methods, you should change this section accordingly and merge it back into the adapter repository.

STEPS  
1. Ensure you have access to a Aruba Central server and that it is running
2. Follow the steps in the README.md to import the adapter into IAP if you have not already done so
3. Use the properties below for the ```properties.authentication``` field
```json
"authentication": {
  "auth_method": "static_token",
  "token": "token",
  "auth_field": "header.headers.Authorization",
  "auth_field_format": "Bearer {token}",
  "auth_logging": false
}
```
you can leave all of the other properties in the authentication section, they will not be used for static token authentication.

4. Restart the adapter. If your properties were set correctly, the adapter should go online. 

### Multi Step Authentication
The Aruba Central adapter also supports Multi Step Authentication. You can read more about Aruba Central OAuth <a href="https://developer.arubanetworks.com/hpe-aruba-networking-central/docs/api-oauth-access-token" target="_blank">here</a>. If you change authentication methods, you should change this section accordingly and merge it back into the adapter repository.

STEPS  
1. Ensure you have access to a Aruba Central server and that it is running
2. Follow the steps in the README.md to import the adapter into IAP if you have not already done so
3. Use the properties below for the ```properties.authentication``` field
```json
"authentication": {
  "auth_method": "multi_step_authentication",
  "multiStepAuthCalls": [
    {
      "name": "getFirstToken",
      "requestFields": {
        "query.client_id": "<your_client_id>",
        "username": "<your_username>",
        "password": "<your_password>"
      },
      "responseFields": {
        "csrftoken": "set-cookie.csrftoken",
        "session": "set-cookie.session"
      },
      "successfullResponseCode": 200
    },
    {
      "name": "getSecondToken",
      "requestFields": {
        "query.client_id": "<your_client_id>",
        "query.response_type": "code",
        "query.scope": "all",
        "header.Cookie": "session={getFirstToken.responseFields.session}",
        "header.X-CSRF-Token": "{getFirstToken.responseFields.csrftoken}",
        "customer_id": "<your_customer_id>"
      },
      "responseFields": {
        "authCode": "auth_code"
      },
      "successfullResponseCode": 200
    },
    {
      "name": "getThirdToken",
      "requestFields": {
        "client_id": "<your_client_id>",
        "client_secret": "<your_client_secret>",
        "grant_type": "authorization_code",
        "code": "{getSecondToken.responseFields.authCode}"
      },
      "responseFields": {
        "token": "access_token"
      },
      "successfullResponseCode": 200
    }
  ],
  "token_timeout": 100000,
  "token_cache": "local",
  "invalid_token_error": 401,
  "auth_field": "header.headers.Authorization",
  "auth_field_format": "Bearer {token}",
  "auth_logging": true
}
```

4. Use the properties below for the ```properties.authentication.refresh_token_request``` field if you want to refresh the access token when it expires, instead of making multiple requests to obtain a new access token
```json
"refresh_token_request": {
  "requestFields": {
    "query.client_id": "<your_client_id>",
    "query.client_secret": "<your_client_secret>",
    "query.grant_type": "refresh_token"
  },
  "refresh_token": {
    "placement": "query",
    "token_timeout": 1296000000
  }
}
```

5. Restart the adapter. If your properties were set correctly, the adapter should go online. 

### Troubleshooting
- Make sure you copied over the correct token.
- Turn on debug level logs for the adapter in IAP Admin Essentials.
- Turn on auth_logging for the adapter in IAP Admin Essentials (adapter properties).
- Investigate the logs - in particular:
  - The FULL REQUEST log to make sure the proper headers are being sent with the request.
  - The FULL BODY log to make sure the payload is accurate.
  - The CALL RETURN log to see what the other system is telling us.
- Credentials should be ** masked ** by the adapter so make sure you verify the username and password - including that there are erroneous spaces at the front or end.
- Remember when you are done to turn auth_logging off as you do not want to log credentials.
