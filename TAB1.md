# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Aruba_central System. The API that was used to build the adapter for Aruba_central is usually available in the report directory of this adapter. The adapter utilizes the Aruba_central API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Aruba Central adapter from Itential is used to integrate the Itential Automation Platform (IAP) with HPE Aruba Networking Central. 

With this adapter you have the ability to perform operations with Aruba Central such as:

- Create Site
- Get Site
- Get Network
- Get Transport Profile
- List Access Points
- Assign License
- Get Network Health

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
