
## 2.0.0 [01-11-2023]

* ADAPT/2478

See merge request itentialopensource/adapters/security/adapter-aruba_central!3

---

## 1.1.0 [11-02-2022]

* Minor/update auth endpoint

See merge request itentialopensource/adapters/security/adapter-aruba_central!2

---

## 1.0.0 [10-28-2022]

* Add new calls

See merge request itentialopensource/adapters/security/adapter-aruba_central!1

---
