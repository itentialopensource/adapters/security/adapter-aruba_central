
## 2.2.6 [10-15-2024]

* Changes made at 2024.10.14_19:49PM

See merge request itentialopensource/adapters/adapter-aruba_central!16

---

## 2.2.5 [10-08-2024]

* Add config for msa and refresh token

See merge request itentialopensource/adapters/adapter-aruba_central!14

---

## 2.2.4 [08-23-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-aruba_central!13

---

## 2.2.3 [08-14-2024]

* Changes made at 2024.08.14_17:56PM

See merge request itentialopensource/adapters/adapter-aruba_central!12

---

## 2.2.2 [08-06-2024]

* Changes made at 2024.08.06_19:10PM

See merge request itentialopensource/adapters/adapter-aruba_central!11

---

## 2.2.1 [08-06-2024]

* Changes made at 2024.08.06_12:52PM

See merge request itentialopensource/adapters/adapter-aruba_central!10

---

## 2.2.0 [05-14-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/security/adapter-aruba_central!9

---

## 2.1.4 [03-27-2024]

* Changes made at 2024.03.27_13:37PM

See merge request itentialopensource/adapters/security/adapter-aruba_central!8

---

## 2.1.3 [03-13-2024]

* Changes made at 2024.03.13_13:46PM

See merge request itentialopensource/adapters/security/adapter-aruba_central!7

---

## 2.1.2 [03-11-2024]

* Changes made at 2024.03.11_16:12PM

See merge request itentialopensource/adapters/security/adapter-aruba_central!6

---

## 2.1.1 [02-27-2024]

* Changes made at 2024.02.27_11:44AM

See merge request itentialopensource/adapters/security/adapter-aruba_central!5

---

## 2.1.0 [01-03-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-aruba_central!4

---

## 2.0.0 [01-11-2023]

* ADAPT/2478

See merge request itentialopensource/adapters/security/adapter-aruba_central!3

---

## 1.1.0 [11-02-2022]

* Minor/update auth endpoint

See merge request itentialopensource/adapters/security/adapter-aruba_central!2

---

## 1.0.0 [10-28-2022]

* Add new calls

See merge request itentialopensource/adapters/security/adapter-aruba_central!1

---
